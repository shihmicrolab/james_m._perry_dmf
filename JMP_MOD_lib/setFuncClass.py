import pyvisa as visa
'''
python 3 convert date: 2/March/2020
'''

class setFunc:
    def __init__(self,initVolts=2,dispVolts=2.5,scanVolts=1, UDP_send = False):
        
        """Initialize our function generator, connected via usb"""
        self.UDP_send = UDP_send
        try:
            self.rm = visa.ResourceManager()
        except ValueError:
            print('No pyVISA device available')
            return
        self.fg_id = (self.rm.list_resources())[0]
        if self.UDP_send == False:
            print('Using device: %s'%(self.fg_id))
        else:
            self.UDP_send('Using device: %s'%(self.fg_id))
        self.fg = self.rm.open_resource(self.fg_id)
        print('Function Generator Connected')
        self.initVolts = initVolts
        self.setFunc()
        self.dispVolts = dispVolts
        self.scanVolts = scanVolts
        if self.dispVolts > 3:
            self.dispVolts = 3
            if self.UDP_send == False:
                print('voltage too high, volts=3')
            else:
                self.UDP_send('voltage too high, volts=3')
            

    def setFunc(self, func='SIN', freq=15000, volts=2):
        self.func = func
        self.freq = freq
        self.volts = volts
        self.set='APPL:%s %s,%s'%(self.func, self.freq, self.volts)
        
        if self.UDP_send == False:
            print((self.set))
        else:
            self.UDP_send((self.set))
            
        self.fg.write(self.set)

if __name__ == '__main__':
    voltage=setFunc()

