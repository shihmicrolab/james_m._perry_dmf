#!/usr/bin/python
'''
python 3 convert date: 2/March/2020
'''
import socket


class UDP_client():
    def __init__(self, IP = '127.0.0.1', port = 8830):
        self.my_socket = socket.socket()
        self.IP = IP
        self.port = port
        self.my_socket.connect((self.IP, self.port))
        
    def send(self,message):
        message = str(message)
        self.my_socket.send(message.encode())
        
##UDP1 = UDP_client(IP = '127.0.0.1', port = 8820)
##UDP2 = UDP_client(IP = '127.0.0.1', port = 8830)
##
##UDP1.send('hello')
##UDP2.send('hello')
