'''
python 3 convert date: 2/March/2020
'''
import time
from GSOF_ArduBridge import threadBasic
import client

class sequencer(threadBasic.BasicThread):
    def __init__(self, seqList=[], Period=1.0):

        self.UDP=client.UDP_client()
        self.seqList = seqList
        nameID='sequencer'
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})
        
        self.seqList = []
        self.execList = []
        
    
    def load(self, seq=[]):

        for i in range(len(seq)):
            self.seqList.append([])
            
            self.seqList[i].insert(0,seq[i])
            if self.seqList[-1]==[]:
                self.seqList.remove([])
            self.execute()
            
        
    def execute(self):
        self.execList=self.seqList
    
    def process(self):
         
        if len(self.execList) != 0:
            tele=self.execList[0]
##            telefile = open('tele.txt','a')
##            telefile.write('%s\n'%(tele))
##            telefile.close()
            self.execList=self.execList[1:]
            self.seqList=self.execList
            self.UDP.send(str(tele))
            #print(tele)

test = sequencer()
