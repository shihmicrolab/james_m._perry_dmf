 
import time
import math
import random
import threading
from GSOF_ArduBridge import threadBasic
import movAvg


class evpRan:
    def __init__(self,avgEvp=-0.10, var=0.012):
        self.avgEvp = avgEvp
        self.var = var
    def out(self):
        self.newEvp = self.avgEvp + random.random()*self.var
        return self.newEvp

class dropRan:
    def __init__(self, avgDrop=200, var=15, rel=0.9):
        self.avgDrop = avgDrop
        self.newDrop = avgDrop
        self.var = var
        self.disp = False
        self.rel = rel
    def out(self):
        if random.random() < self.rel:
            self.newDrop = self.avgDrop + random.random()*self.var
            self.disp = True

class electVol(threadBasic.BasicThread):
    def __init__(self,
                 electList=11,
                 impScan=False,
                 dispFunc=False,src=1, ori=0, des=2, orif=0,
                 flipFunc=False,flipArg=[6,1,6,0],
                 Period=2,
                 setPoint=700,
                 nameID='volMon',
                 UDP_send = False,
                 adaptPeriod = False,
                  sideChain=False,
                 syringe=False,
                 simMode =False,
                 PID = False,
                 saveDispImgFunc = False,
                 saveMeasImgFunc = False,
                 saveFlipImgFunc = False,
                 redoN=2):
        
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})
        
        if type(electList) == 'int':
            electList = [electList]
        self.electList = electList
        self.impScan = impScan
        self.setVol(setPoint)
        self.elecImp = 0.0
        self.z1 = 0.0
        
        self.saveDispImgFunc = saveDispImgFunc
        self.saveMeasImgFunc = saveMeasImgFunc
        self.saveFlipImgFunc = saveFlipImgFunc
        self.dispFunc = dispFunc
        self.src = src
        self.ori = ori
        self.des = des
        self.orif = orif
        self.flipFunc = flipFunc
        self.flipArg = flipArg
        self.rate = 0.0

        self.UDP_send = UDP_send
        
        self.avgEvpRate = movAvg.movAvg(init=(-50/60)*3, N=3)
        self.avgDrop = movAvg.movAvg(init=100, N=3)
        self.avgVol = movAvg.movAvg(init=self.setPoint, N=2)

        self.adaptPeriod = adaptPeriod
        self.sideChain = sideChain
        self.syringe = syringe
        self.syrStep = 0.5
        self.simMode = simMode
        self.setPeriod = self.Period
        self.PID = PID
        self.temp = 0.0
        self.tempChange = False

        self.redoN = redoN
        self.redo = self.redoN
        self.dispNum=0
        self.dispError=0
        self.dispRel = 0
        if self.dispNum != 0:
            self.dispRel=((self.dispNum-self.dispError)/self.dispNum)

        self.evp = evpRan()
        self.drop = dropRan()

        #Resorvoir and droplet volume estimation
        self.rateEst = -50.0/60.0
        self.ResEst = 0.0
        self.pError = 0.0
        self.pCalc = 0.0
        self.pEvap = 0.0
        self.measP = 60.0

        self.resLimit=self.setPoint*1.15
        self.upperLimit = -12
        self.lowerLimit = -1
        self.timeStart = time.time()
        self.timeThen = time.time()
        
        self.timer = 0.0
        self.disp = False
        self.dispTimer = 0.0
        self.syrDisp = False
        self.syrDispTime = 3.0
        self.syrDispTimer = 0.0
        self.syrKp = -0.035
        self.propBand = 1.0
        self.syrKi = -0.0001
        self.dSum=0
        self.intTerm=0
        self.syrKd = -10
        self.DerivTerm = 0
        
  
    def setVol(self, val=-1):
        self.setPoint = val
        if val==-1:
            self.setPoint = sum(self.impScan(self.electList))
        return self.setPoint

    def meas(self):
        self.elecImp = sum(self.impScan(self.electList))
        while self.elecImp < 0:
            self.elecImp = sum(self.impScan(self.electList))
        self.error = self.setPoint -self.avgDrop.out/2 -self.elecImp 
        return [self.elecImp, self.error]

    def trigger(self):
        self.process()

    def process(self):
            time.sleep(1)
            self.sideChain()

            ###Main###
            self.UDP_send( 'Enter: Period %3.2f'%(self.Period))
            
            #Time calculations
            self.timeNow = time.time() 
            self.measP = self.timeNow - self.timeThen
            self.timer += self.measP
            self.timeThen = self.timeNow
            
            if self.simMode == False:
                self.meas()
                if self.saveMeasImgFunc != False:
                    self.saveMeasImgFunc()
                
            #--- Simulation ---#
            else:
                self.avgEvpRate.step(self.evp.out())
                self.elecImp += self.evp.out()*self.measP
                
                if self.disp == True:
                    self.elecImp += self.drop.newDrop
                    self.disp = False
            
            #--- Algorithm ---#

            
            self.diff = (self.elecImp - self.z1)
            self.rate = self.diff/self.measP
            #Clamp that evpRate 
            if self.rate < -5:
                self.rate = -0.8
                
            if self.disp == True:
                self.dispNum += 1
                #self.disp = False
                if (self.diff > ((self.avgDrop.out)/4)):
                    self.avgDrop.step(self.diff) #- self.measP*self.avgEvpRate.out)
                    self.redo=self.redoN
                else:
                    self.UDP_send( 'dispencing error')
                    self.dispError += 1
                    if self.redo < 0:
                        self.redo = 0
                    
                    
            else:
                if self.rate < 0 and self.rate > -5:
                    self.avgEvpRate.step(self.rate)
                    
            #Count seconds since last disp        
            if self.disp == True:
                self.dispTimer = self.measP
                #self.disp = False ####reset to false####
            else:
                self.dispTimer += self.measP
                    
            #Count seconds since last syringe disp        
            if self.syrDisp == True:
                self.syrDispTimer = self.measP
                self.syrDisp = False ####reset to false####
            else:
                self.syrDispTimer += self.measP
                
            #calsculate threashold (keeps average on the setpoint)    
            self.threshold = self.setPoint - (self.avgDrop.out/2.0)
            #Calculate error
            self.error = self.threshold - self.elecImp

            #average volume 
            self.avgVol.step(self.elecImp)

            teleUpdate=('Elect,%d, time,%3.2f, setpoint,%3.2f, impVal,%3.2f, Temp,%3.2f, Diff, %3.2f, rate,%3.2f, avgDrop,%3.2f, avgEvp,%3.2f, step,%3.2f, upperStepLimit,%3.2f, Period,%3.2f, pCalc,%3.2f, measP,%3.2f, Date,%s\n'%(self.electList,time.time(),self.setPoint,self.elecImp,self.temp,self.diff,self.rate,self.avgDrop.out,self.avgEvpRate.out,self.src,self.upperLimit, self.Period, self.pCalc,self.measP,time.ctime()))
            #print(teleUpdate)
            self.UDP_send(str(teleUpdate))
            #self.teleUpdate(teleUpdate)
            
            csv = open('electrode_volume_Measurements/elecVol#%s.csv'%(self.electList), 'a')
            csvWeb = open('C:/1/teleLOG.csv', 'a')
            csv.write(teleUpdate)
            csvWeb.write(teleUpdate)
            csv.close()
            csvWeb.close()

            ### moving graph ###
            
##            movingGraph = open('movingGraph%s.txt'%(self.electList), 'a')
##            movingGraphUpdate = ('%3.2f,%3.2f\n'%(self.timer,self.elecImp))
##            movingGraph.write(movingGraphUpdate)
##            movingGraph.close()

            #Calculate new period: T
            if self.adaptPeriod == False:
                self.Period = self.setPeriod
            else:
                self.pCalc = self.avgDrop.out/abs(self.avgEvpRate.out)
                self.Period = self.pCalc/2.0
            if self.tempChange == True:
                self.tempChange = False
                self.Period = self.setPeriod/2.0
            #Determine temperature
            if self.PID != False:
                self.temp = self.PID.PID.feedback[0]
            #Calculate new syringe step
            if (self.syringe == True): #and (self.syrDisp == True):
                
                
                #self.syrDisp = False ####reset to false#### 
                
##                if self.avgVol.out < self.threshold:
##                    self.src += (-1)*self.syrStep
##                    if self.src < self.upperLimit:
##                        self.src = self.upperLimit
##                elif self.avgVol > (self.setPoint + self.avgDrop.out):
##                    self.src += (1)*self.syrStep
                    
##                #Autotune syringe
##                if (self.rate < 0) and (self.error > 0):
##                    self.upperLimit += (-1)
##                if (self.elecImp > (self.setPoint)):
##                    self.upperLimit -= (-1)
##                    
##                if (self.rate > 0) and (self.error < 0):
##                    self.upperLimit -= (-1)
##                    if (self.upperLimit > -10):
##                        self.upperLimit = -10
##                if (self.upperLimit < -10):
##                        self.upperLimit = -10
                    
                self.syrKp = self.upperLimit/(self.setPoint*self.propBand)

                
                if self.flipFunc != False:
                    #while self.elecImp > self.setPoint+(self.avgDrop.out):
                    while (self.elecImp > self.resLimit):
                        
                        time.sleep((self.flipFunc(*self.flipArg))*1.2)
                        self.dispFunc(2.0, self.ori, self.des, self.orif)
                        self.meas()
                        time.sleep(1)
                        
                        self.UDP_send('FLIPPING ************ setPoint is %s and elecImp is %s'%(self.setPoint, self.elecImp))
                        if self.saveFlipImgFunc != False:
                            self.saveFlipImgFunc()
                
                #set src value
                self.propTerm = (self.setPoint - self.elecImp) * self.syrKp
                self.dSum = (self.error*self.measP*self.syrKi)
                self.intTerm += self.dSum
                self.DerivTerm = (-self.rate) * self.syrKd
                
                self.src = self.propTerm + self.intTerm + self.DerivTerm
                
                #syringe step clamp    
                if self.src < self.upperLimit:
                    self.src = self.upperLimit
                if self.src > self.lowerLimit:
                    self.src = self.lowerLimit

            #Decide whether to dispense now or next cycle
            errorNow = -self.error
            errorNext = -(self.error -self.avgEvpRate.out*self.Period)
            if (errorNow > 0) and (errorNext > 0):
                self.UDP_send('Disp later')
                    
            else:
                self.disp = False
                if (abs(errorNow) < abs(errorNext)):
                    self.disp = True
                    if self.simMode == False:
                        
                        if (self.syringe == False) and (self.elecImp < (self.setPoint + 200)):
                            self.dispWait=(self.dispFunc(self.src, self.ori, self.des, self.orif))
                            if self.dispWait == 0:
                                if self.sideChain != False:
                                    
                                    print 'disp function returned 0'
                                    self.sideChain()
                                    return
                                
                            time.sleep(float(self.dispWait)*1.5)
                            if self.saveDispImgFunc != False:
                                self.saveDispImgFunc()

                        elif (self.syrDispTimer >= self.syrDispTime):
                            self.UDP_send( 'SYRINGE PUMPPING *********** setPoint is %s and elecImp is %s'%(self.setPoint, self.elecImp))
                            time.sleep((self.dispFunc(self.src, self.ori, self.des, self.orif))*1.2)
                            self.syrDisp = True
                        else:
                            self.UDP_send( 'Syringe disp must wait atleast: %3.2f seconds'%(self.syrDispTime-self.syrDispTimer))
                            
                            print 'syringe wait'
                            self.sideChain()
                            self.sideChain()
                            return
                        
                    else:
                        self.drop.out()  
                    self.UDP_send( 'Disp now' )
                    self.UDP_send( abs(errorNow) )
                    self.UDP_send( abs(errorNext) )
                    
      
            self.z1 = self.elecImp
                
            if (self.disp == True):
                self.disp = False
                
                print 'RETURN: self-triggered'
                self.trigger()
                return
            
                if self.syringe == False:
                    time.sleep(1)
                    if self.redo != 0:
                        self.UDP_send( '############ redo =%s'%(self.redo))
                        self.redo-=1
                        
                        print 'RETURN: out of redos'
                        self.trigger()
                        return

            if self.sideChain != False:
                
                print 'RETURN: end of process'
                self.sideChain()
                return
                
            #safty for fillup    
##            if self.avgVol.out < self.setPoint - 100:
##                self.Period = 20.0
            #print 'Exit: Period %3.2f'%(self.Period)
            ###Main###





##sim3=electVol(17,simMode=True,adaptPeriod=False)
##sim2=electVol(15,simMode=True,adaptPeriod=False,sideChain=sim3.trigger)
##sim=electVol(11,simMode=True,adaptPeriod=False,sideChain=sim2.trigger)
##
