#!/usr/bin/python
'''
python 3 convert date: 2/March/2020
'''
import socket

server_socket = socket.socket()
server_socket.bind(('0.0.0.0', 8830))

server_socket.listen(1)

(client_socket, client_address) = server_socket.accept()

while True:
    client_data = client_socket.recv(1024)
    if client_data == 'close':
        client_socket.close()
        server_socket.close()
    print(("Received: %s" % client_data.decode()))
    
##client_socket.send('Hello ' + client_data + '!')
##
##client_socket.close()
##server_socket.close()
