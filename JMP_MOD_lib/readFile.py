'''
python 3 convert date: 2/March/2020
'''
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Parameters
x_len = 200         # Number of points to display
y_range = [0, 10]  # Range of possible Y values to display

# Create figure for plotting
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = list(range(0, 200))
ys = [0] * x_len
ax.set_ylim(y_range)


# Create a blank line. We will update the line in animate
linex, = ax.plot(xs, ys)

# Add labels
plt.title('TMP102 Temperature over Time')
plt.xlabel('Samples')
plt.ylabel('Temperature (deg C)')

# This function is called periodically from FuncAnimation
def animate(i, ys):

    data = open('movingGraph.txt', 'r')
    line = data.readlines()
    updateTime = float(line[-1].split(',')[0])
    updateImp = float(line[-1].split(',')[1])

    # Add y to list
    ys.append(updateImp)

    # Limit y list to set number of items
    ys = ys[-x_len:]

    # Update line with new Y values
    linex.set_ydata(ys)

    return linex,

# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig,
    animate,
    fargs=(ys,),
    interval=1000,
    blit=True)
plt.show()


