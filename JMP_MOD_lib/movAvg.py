'''
python 3 convert date: 2/March/2020
'''
class movAvg():
    def __init__(self, init=0, N=4):
        init = float(init)
        N = int(N)
        self.N = N
        self.out = init/N
        self.array = [self.out]*N
        self.sum = init
        self.idx = 0
        
    def step(self, newVal):
        self.sum -= self.array[self.idx]
        self.sum += newVal
        self.array[self.idx] = newVal
        self.idx += 1
        if self.idx >= self.N:
            self.idx = 0
        self.out = self.sum / self.N
        return self.out
