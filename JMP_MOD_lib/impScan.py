'''
python 3 convert date: 2/March/2020
'''
import time 

class impScan:
    def __init__(self,
                 ExtGpio=False,
                 ardu=False,
                 setFunc=False
                 ):
        self.ExtGpio = ExtGpio
        self.ardu = ardu
        self.setFunc = setFunc
        self.voltage = self.setFunc
        self.initVolts = 2
        self.dispVolts = 2.5
        self.scanVolts = 1
        self.calibration = False
        self.m=[]
        self.b=[]
        
       

    def impScan(self,elects,retry=3):
        imp = []
        if type(elects) != 'int':
            elects = [elects]
        for elect in elects:
            self.ExtGpio.pinWrite(elect, 1)
            time.sleep(.5)
            self.setFunc.setFunc(volts=self.setFunc.scanVolts)
            time.sleep(.5)
            val = -1
            while (retry > 0) and ((val > 1023) or (val < 0)):
                val = self.ardu.an.analogRead(1)
                print('invalid impScan val: Retry')
                retry -= 1
                    
            imp.append(val)
            print('elec# %d, %d'%(elect, val))
            self.setFunc.setFunc(volts=self.setFunc.initVolts)
            self.ExtGpio.pinWrite(elect, 0)

        if self.calibration == True:
            imp = [int((sum(imp) - self.bVal)/self.mVal)]
        return imp

    def calib(self,elects,vol=0,impInput=0):
        """Calibration of impVal [bin] to Vol [nL]"""
        self.calibration = False
        if impInput != 0:
            self.impVal=impInput
        self.impVal = self.impScan(elects)[0]
        time.sleep(2)

        self.a = -87.309
        self.c = -7.4465
        self.m.append((self.impVal-self.c)/(vol+self.a))
        self.mVal=(sum(self.m)/len(self.m))
        self.b.append(self.a*self.mVal+self.c)
        self.bVal = (sum(self.b)/len(self.b))        
        self.calibration = True
        print('calibrated with %d value(s)'%(len(self.b)))
        
    def resetCalib(self):
        self.calibration = False
        self.b=[]
        self.bVal=0
        self.m=[]
        self.mVal=0
        
