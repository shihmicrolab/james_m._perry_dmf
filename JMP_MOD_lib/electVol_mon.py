'''
Python 3 convert Date: 2/March/2020 
'''

import time
import random
from GSOF_ArduBridge import threadBasic
import JMP_MOD_lib.movAvg

class evpRan:
    def __init__(self,avgEvp=-0.5, var=0.012):
        self.avgEvp = avgEvp
        self.var = var
    def out(self):
        self.newEvp = self.avgEvp + random.random()*self.var
        return self.newEvp

class dropRan:
    def __init__(self, avgDrop=100, var=15, rel=0.9):
        self.avgDrop = avgDrop
        self.newDrop = avgDrop
        self.var = var
        self.disp = False
        self.rel = rel
    def out(self):
        if random.random() < self.rel:
            self.newDrop = self.avgDrop + random.random()*self.var
            self.disp = True

class electVol(threadBasic.BasicThread):
    def __init__(self,
                 electList=11,
                 impScan=False,
                 dispFunc=False,src=1, ori=0, des=2, orif=0,
                 Period=60,
                 setPoint=700,
                 nameID='volMon',
                 sideChain=False,
                 simMode =False):
        
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})
        
        if type(electList) == 'int':
            electList = [electList]
        self.electList = electList
        self.setVol(setPoint)
        self.impScan = impScan
        self.elecImp = 700.0
        self.z1 = 700.0
        self.dispFunc = dispFunc
        self.src = src
        self.ori = ori
        self.des = des
        self.orif = orif
        self.rate = 0.0
        
        self.avgEvpRate = movAvg.movAvg(init=(-50/60)*3, N=3)
        self.avgDrop = movAvg.movAvg(init=300, N=3)
        self.avgVol = movAvg.movAvg(init=self.setPoint, N=3)
        
        self.sideChain = sideChain
        self.simMode = simMode
        self.evp = evpRan()
        self.drop = dropRan()

        #Resorvoir and droplet volume estimation
        self.rateEst = -50.0/60.0
        self.ResEst = 0.0
        self.pError = 0.0
        self.pCalc = 0.0
        self.pEvap = 0.0
        
        self.timer = 0.0
        self.disp = False
  
    def setVol(self, val=-1):
        self.setPoint = val
        if val==-1:
            self.setPoint = sum(self.impScan(self.electList))
        return self.setPoint

    def meas(self):
        self.elecImp = sum(self.impScan(self.electList))
        self.error = self.setPoint -self.avgDrop.out/2 -self.elecImp 
        return [self.elecImp, self.error]

    def trigger(self):
        self.process()

    def process(self):
        self.timer += self.Period
        
        if self.simMode == False:
            self.meas()
            
        #--- Simulation ---#
        else:
            self.avgEvpRate.step(self.evp.out())
            self.elecImp += self.evp.out()*self.Period
            
            if self.disp == True:
                self.elecImp += self.drop.newDrop
                self.disp = False
        
        #--- Algorithm ---#

        
        self.diff = (float(self.elecImp) - float(self.z1))
        self.rate = self.diff/self.Period
        if self.disp == True:
            if self.diff > 0:
                self.avgDrop.step(self.diff - self.Period*self.avgEvpRate.out)
            else:
                print('dispencing error')
        else:
            if self.rate < 0:
                self.avgEvpRate.step(self.rate)
            
        self.threshold = self.setPoint - (self.avgDrop.out/2.0)
        self.error = self.threshold - self.elecImp

        #average volume 
        self.avgVol.step(self.elecImp)

        teleUpdate=('Elect,%3.2f,%3.2f, %3.2f,diff,%3.2f, rate,%3.2f, avgDrop,%3.2f, avgEvp,%3.2f, Period,%3.2f, pCalc,%3.2f\n'%(self.electList,self.timer,self.elecImp,self.diff,self.rate,self.avgDrop.out,self.avgEvpRate.out,self.Period,self.pCalc))
        print(teleUpdate)
        self.teleUpdate(teleUpdate)
        csv = open('NEW-CODE.csv','a')
        csv.write(teleUpdate)
        csv.close()


        self.z1 = self.elecImp
        
        if self.sideChain != False:
            self.sideChain()
