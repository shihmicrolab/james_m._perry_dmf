"""
Script to run a while loop for scanning electrode impedance
python 3 convert date: 2/March/2020
"""

class imped():
    def __init__(self,elect,period,):
        self.elect = elect
        self.period = period

    def impScan(elects):
        imp = []
        if type(elects) != 'int':
            elects = [elects]
        for elect in elects:
            ExtGpio.pinWrite(elect, 1)
            time.sleep(.5)
            V = ardu.an.analogRead(1)
            imp.append(V)
            print('elec# %d, %d'%(elect, V))
            #time.sleep(.5)
            ExtGpio.pinWrite(elect, 0)
        return imp
