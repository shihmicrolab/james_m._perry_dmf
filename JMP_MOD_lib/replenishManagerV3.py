'''
python 3 convert date: 2/March/2020
'''
import time
import threading
from GSOF_ArduBridge import threadBasic
from JMP_MOD_lib import client
#import client

import sys


class priorityQueue(threadBasic.BasicThread):
    def __init__(self,
                 objList = [],
                 PID = False,
                 leader = False,
                 queue = False,
                 totalVal = 0,
                 #UDP_send = False,
                 nameID='priorityQueue',
                 Period = 10,
                 viewer={},
                 selfPop = False
                 ):
        self.objList = objList
        self.PID = PID
        self.leader = leader
        self.totalVal = totalVal
        self.queue = []
        self.selfPop = selfPop
        self.counter = 0
        #self.UDP_send = UDP_send
    
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer=viewer)
        
    def rank(self, i):
        return i[1]
    
    def survey(self):
        self.totalVal = 0
        #print("Total Val 0!!!!: "+str(self.totalVal))
        for i in self.objList:
            imp = (i.meas()[0])
            sp = (i.setPoint)
            val = (imp/sp)
            self.teleUpdate( i.name + ': ' + '%3.2f'%(val))
            self.queue.append([i,val])
            time.sleep(0.2) ### experimental
            self.totalVal += val
        #self.queue.sort(key = self.rank, reverse = True)
        self.queue.sort(key = self.rank)
        
##        if self.leader != False:
##            leader = self.leader
##            leaderVal = leader.meas()[1]
##            for i in range(len(self.queue)):
##                #self.queue.insert(i+i,[leader,leaderVal])
##                self.queue.insert(i+i,[leader,0])
                
##    def surveyImp(self):
##        self.totalVal = 0
##        #print("Total Val 0!!!!: "+str(self.totalVal))
##        for i in self.objList:
##            val = (i.error)
##            self.teleUpdate( i.name + ': ' + '%3.2f'%(val))
##            self.queue.append([i,val])
##            time.sleep(0.2) ### experimental
##            self.totalVal += val
##        self.queue.sort(key = self.rank, reverse = True)      
##        if self.leader != False:
##            leader = self.leader
##            leaderVal = leader.meas()[1]
##            for i in range(len(self.queue)):
##                #self.queue.insert(i+i,[leader,leaderVal])
##                self.queue.insert(i+i,[leader,0])
            
    def trigger(self):
        self.process()

    def process(self):
        for i in self.queue:
            self.teleUpdate( i[0].name + ': ' + '%3.2f'%(i[1]))
        if self.PID != False:
            if self.PID.enable == True:
                self.teleUpdate("PID running\n")
                if self.PID.ctrl_Settle == -1:
                    self.teleUpdate("Temperature change, skip period\n")
                    return
        #print("Total Val: "+str(self.totalVal))
        t = time.time()
        self.counter += 1
        if len(self.queue) != 0:
            self.totalVal -= self.queue[0][1]
            
            self.teleUpdate('working on res')
            self.leader.trigger()
            self.teleUpdate('Done res')
            
            job = self.queue[0][0]
            #print("Job: "+str(job))
            self.teleUpdate('working on %s. Old val: %s'%(job.name,job))
            job.trigger()
            self.teleUpdate('Done Trigger')
            self.queue[0][1] = self.queue[0][0].elecImp/self.queue[0][0].setPoint
            #self.queue.sort(key = self.rank, reverse = True)
            self.queue.sort(key = self.rank)

            if self.selfPop != False:
                self.queue.pop(0)[0]
            
        else:
            self.teleUpdate('Queue empty, Surveying')
            self.survey()
            self.lock.release()
        if self.counter > 3:
            self.queue = []
            self.counter = 0

if __name__ == "__main__":
    
### the following is a fun example thread used for unit testing priorityQueue
    
    class dog(threadBasic.BasicThread):
        """Class to unit test priorityQueue objects"""
        def __init__(self,age,bark,pqman=False,nameID='DogThread',Period = 0):
            self.age = age
            self.bark = bark
            self.nameID = nameID
            self.pqman = pqman
            threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})
            
        def letsGo(self,n=3):
            print(str(self.bark*n))
            for i in range(10):
                print(i)
                time.sleep(1)
        
                
        def meas(self):
            return [self.name,self.age]
           
        def trigger(self):
            self.process()
            
        def process(self):
            self.pqman.lock.acquire(1)
            self.letsGo()
            self.pqman.lock.release()

    class PID():
        def __init__(self):
            self.enable = True
            self.ctrl_settle = -1
            
    PID = PID()
    UDP =client.UDP_client(IP = '127.0.0.1', port = 8830)
    PQ = priorityQueue(teleUpdate = UDP.send, PID = PID)

    scruffy = dog(12,'woof ',pqman=PQ,nameID ='scruffy')
    muffy = dog(2, 'ruff ',pqman=PQ,nameID ='muffy')
    puffy = dog(6, 'broof ',pqman=PQ,nameID ='puffy')
    fluffy = dog(9, 'huff ',pqman=PQ,nameID ='fluffy')
    checkers = dog(11, 'arf', pqman=PQ,nameID ='checkers')
    dogs = [scruffy, muffy, puffy, fluffy]

    PQ.objList = dogs
    PQ.leader = checkers
    PQ.start()
