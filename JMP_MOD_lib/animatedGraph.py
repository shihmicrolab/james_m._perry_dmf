import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import datetime as dt

import pandas_datareader.data as web
style.use('seaborn-pastel')

class AnimatedGraph():
    def __init__(self,xdata,ydataList=[],title='Name', xlabel='time', ylabel='value'):
        
       self.xdata = xdata
       self.ydataList = ydataList
       self.title = title
       self.xlabel = xlabel
       self.ylabel = ylabel

       self.plt = plt
       
       self.fig = self.plt.figure(self.title)
       self.ax1 = self.fig.add_subplot(1,1,1)
       self.ani = animation.FuncAnimation(self.fig, self.animate, interval=1000)
       #self.plt.ion()
       self.plt.show()


    def animate(self,i):
        self.ax1.clear()
        for y in self.ydataList:
            self.plt.title(self.title)
            self.plt.xlabel(self.xlabel)
            self.plt.ylabel(self.ylabel)
            self.ax1.plot(self.xdata, y)
            

            

