'''
python 3 convert date: 2/March/2020
'''
import time
import threading
from GSOF_ArduBridge import threadBasic
#from JMP_MOD_lib import client
import client
import queue
import sys


class priorityQueue(threadBasic.BasicThread):
    def __init__(self,
                 objList = [],
                 leader = False,
                 lineup = False,
                 UDP_send = False,
                 nameID='priorityQueue',
                 Period = 2
                 ):
        self.objList = objList
        self.leader = leader
        self.lineup = queue.PriorityQueue()
        self.UDP_send = UDP_send
    
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})

    def survey(self):
        for i in self.objList:
            val = -(i.meas()[1])
            self.UDP_send( i.name + ': ' + str(-val))
            if self.leader != False:
                leader = self.leader
                self.lineup.put([leader.meas,leader])
            self.lineup.put([val,i])
            time.sleep(1)
            
    def trigger(self):
        self.process()

    def process(self):
        t = time.time()
        if self.lineup.empty() == False:
            job = self.lineup.get()[1]
            self.UDP_send('working on %s'%(job.name))
            job.trigger()
            
        else:
            self.UDP_send('Queue empty, Surveying')
            self.survey()
            self.lock.release()

if __name__ == "__main__":
    
### the following is a fun example thread used for unit testing priorityQueue
    
    class dog(threadBasic.BasicThread):
        """Class to unit test priorityQueue objects"""
        def __init__(self,age,bark,pqman=False,nameID='DogThread',Period = 0):
            self.age = age
            self.bark = bark
            self.nameID = nameID
            self.pqman = pqman
            threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer={})
            
        def letsGo(self,n=3):
            print(str(self.bark*n))
            for i in range(10):
                print(i)
                time.sleep(1)
        
                
        def meas(self):
            return [self.name,self.age]
           
        def trigger(self):
            self.process()
            
        def process(self):
            self.pqman.lock.acquire(1)
            self.letsGo()
            self.pqman.lock.release()
            
            
    UDP =client.UDP_client(IP = '127.0.0.1', port = 8830)
    PQ = priorityQueue(UDP_send = UDP.send)

    scruffy = dog(12,'woof ',pqman=PQ,nameID ='scruffy')
    muffy = dog(2, 'ruff ',pqman=PQ,nameID ='muffy')
    puffy = dog(6, 'broof ',pqman=PQ,nameID ='puffy')
    fluffy = dog(9, 'huff ',pqman=PQ,nameID ='fluffy')
    checkers = dog(11, 'arf', pqman=PQ,nameID ='checkers')
    dogs = [scruffy, muffy, puffy, fluffy]

    PQ.objList = dogs
    PQ.leader = checkers
    PQ.start()
