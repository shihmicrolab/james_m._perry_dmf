#!/usr/bin/python
'''
python 3 convert date: 2/March/2020
'''
import time
import math
import random
import threading
from GSOF_ArduBridge import threadBasic
import JMP_MOD_lib.movAvg as movAvg
import JMP_MOD_lib.replenishManager_py3 as RM
import JMP_MOD_lib.client


class evpRan:
    def __init__(self,avgEvp=-0.1, var=0.0):
        self.avgEvp = avgEvp
        self.var = var
    def out(self):
        self.newEvp = self.avgEvp + random.random()*self.var
        return self.newEvp

class dropRan:
    def __init__(self, avgDrop=200, var=15, rel=0.9):
        self.avgDrop = avgDrop
        self.newDrop = avgDrop
        self.var = var
        self.disp = False
        self.rel = rel
    def out(self):
        if random.random() < self.rel:
            self.newDrop = self.avgDrop + random.random()*self.var
            self.disp = True

class electVol(threadBasic.BasicThread):
    def __init__(self,
                 electList=11,
                 impScan=False,
                 dropSize = 52,
                 dispFunc=False,src=1, ori=0, des=2, orif=0,
                 flipFunc=False,flipArg=[6,1,6,0],
                 mixFunc = False, 
                 Period=5,
                 setPoint=700,
                 nameID='volMon',
                 #udp_send = False,
                 adaptPeriod = False,
                 pqman=False,
                 syringe=False,
                 simMode =False,
                 PID = False,
                 saveDispImgFunc = False,
                 saveMeasImgFunc = False,
                 saveFlipImgFunc = False,
                 redoN=2,
                 viewer={}):
        
        threadBasic.BasicThread.__init__(self, nameID, Period=Period, viewer=viewer)
        
        if type(electList) == 'int':
            electList = [electList]
        self.electList = electList
        self.impScan = impScan
        self.dropSize = dropSize
        self.setVol(setPoint)
        self.elecImp = 0.0
        self.z1 = 0.0
        
        self.saveDispImgFunc = saveDispImgFunc
        self.saveMeasImgFunc = saveMeasImgFunc
        self.saveFlipImgFunc = saveFlipImgFunc
        self.dispFunc = dispFunc
        self.src = src
        self.ori = ori
        self.des = des
        self.orif = orif
        self.flipFunc = flipFunc
        self.flipArg = flipArg
        self.mixFunc = mixFunc
        self.rate = 0.0

        #self.udp_send = UDP_send
        
        self.avgEvpRate = movAvg.movAvg(init=(-50/60)*3, N=3)
        self.avgDrop = movAvg.movAvg(init=100, N=3)
        self.avgVol = movAvg.movAvg(init=self.setPoint, N=2)

        self.adaptPeriod = adaptPeriod
        self.pqman = pqman
        self.syringe = syringe
        self.syrStep = 0.5
        self.simMode = simMode
        self.setPeriod = self.Period
        self.PID = PID
        self.temp = 0.0
        self.tempChange = False

        self.redoN = redoN
        self.redo = self.redoN
        self.dispNum=0
        self.dispError=0
        self.dispRel = 0

        self.evp = evpRan()
        self.drop = dropRan()

        #Resorvoir and droplet volume estimation
        self.rateEst = -50.0/60.0
        self.ResEst = 0.0
        self.pError = 0.0
        self.pCalc = 0.0
        self.pEvap = 0.0
        self.measP = 60.0

        self.resLimit=self.setPoint*1.15
        self.upperLimit = -12
        self.lowerLimit = -1
        self.timeStart = time.time()
        self.timeThen = time.time()
        self.triggerState = False
        
        self.timer = 0.0
        self.disp = False
        self.syrDisp = False
        self.syrDispTime = 40.0
        self.syrDispTimer = 0.0
        self.syrKp = -0.035
        self.propBand = 1.0
        self.syrKi = -0.0001
        self.dSum=0
        self.intTerm=0
        self.syrKd = -10
        self.DerivTerm = 0
        
  
    def setVol(self, val=-1):
        self.setPoint = val
        if val==-1:
            self.setPoint = sum(self.impScan(self.electList))
        return self.setPoint

    def meas(self):
        if self.simMode == False:
            self.elecImp = sum(self.impScan(self.electList))
            while self.elecImp < 0:
                self.elecImp = sum(self.impScan(self.electList))
        else:
            self.simGen()
            self.simMeas()
        #self.error = self.setPoint -self.avgDrop.out/2 -self.elecImp
        self.error = self.setPoint - self.elecImp ###EXPERIMENTAL
        return [self.elecImp, self.error]
    
    def timeCalc(self):
        
        #Time calculations
        self.timeNow = time.time() 
        self.measP = self.timeNow - self.timeThen
        self.timer += self.measP
        self.timeThen = self.timeNow

    #--- Simulation ---#
    def simGen(self):
        '''generate simulated evaporation and set new elecImp'''
        self.avgEvpRate.step(self.evp.out())
        self.elecImp += self.evp.out()*self.measP
        
    def simMeas(self):
        if self.disp == True:
            self.elecImp += self.drop.newDrop
            self.disp = False
        else:
            return
    #--- ---------- ---#

    def rateCalc(self):
        self.diff = (self.elecImp - self.z1)
        self.rate = self.diff/self.measP
        #Clamp rate 
        if self.rate < -5:
            self.rate = -0.8

    def calcSyrStep(self):
        '''Calculate new syringe step (src)'''           
        self.syrKp = self.upperLimit/(self.setPoint*self.propBand)
        self.propTerm = (self.setPoint - self.elecImp) * self.syrKp
        self.dSum = (self.error*self.measP*self.syrKi)
        self.intTerm += self.dSum
        self.DerivTerm = (-self.rate) * self.syrKd
        self.src = self.propTerm + self.intTerm + self.DerivTerm
        #syringe step clamp    
        if self.src < self.upperLimit:
            self.src = self.upperLimit
        if self.src > self.lowerLimit:
            self.src = self.lowerLimit
        
            
    def resOverflow(self):
        '''loop to eliminate excess water from overfilled.'''
        if self.flipFunc != False:
            
            if (self.elecImp > self.resLimit):
                for i in self.pqman.queue:
                    self.teleUpdate('elec errors'+str(i[1]))
                    if i[1] < 0.7:
                        self.teleUpdate('Someone is thirsty')
                        self.pqman.lock.release()
                        #self.threadExit()
                        return
                        
                
                while (self.elecImp > self.resLimit):
                    time.sleep((self.flipFunc(*self.flipArg))+0.5)
                    self.dispFunc(2.0, self.ori, self.des, self.orif)
                    self.meas()
                    time.sleep(.2)
                    s = 'FLIPPING - setPoint is %s and elecImp is %s'%(self.setPoint, self.elecImp)
                    #self.UDP_send(s)
                    self.teleUpdate(s)
                    if self.saveFlipImgFunc != False:
                        self.saveFlipImgFunc()
                        
        else:
            s = '%s does not have an assigned flip function'%(self.name)
            #self.UDP_send(s)
            self.teleUpdate(s)

    def dispVerify(self):
        '''Detect if a droplet actually made it to the electrode'''
        if (self.diff > ((self.avgDrop.out)/4)):
            if self.diff > (self.dropSize * 1.7):
                self.avgDrop.step(self.diff/2)

            else:
                self.avgDrop.step(self.diff) 
            self.redo=self.redoN
            if self.dispNum != 0:
                self.dispRel=((self.dispNum-self.dispError)/self.dispNum)
        else:
            
            s = ('dispencing error')
            #self.UDP_send(s)
            self.teleUpdate(str(self.name)+": "+s)
            
            self.dispError += 1
            if self.redo < 0:
                self.redo = 0

    def graph(self):
        ''' moving graph '''
        movingGraph = open('movingGraph%s.txt'%(self.electList), 'a')
        movingGraphUpdate = ('%3.2f,%3.2f\n'%(self.timer,self.elecImp))
        movingGraph.write(movingGraphUpdate)
        movingGraph.close()

    def calcPeriod(self):
        '''Calculate the new period value'''
        if self.adaptPeriod == False:
            self.Period = self.setPeriod
        else:
            self.pCalc = self.avgDrop.out/abs(self.avgEvpRate.out)
            self.Period = self.pCalc/2.0
        if self.tempChange == True:
            self.tempChange = False
            self.Period = self.setPeriod/2.0

    def tele(self):
        tele=('\nElect,%d, time,%3.2f, setpoint,%3.2f, impVal,%3.2f,'
            'Temp,%3.2f, Diff, %3.2f, rate,%3.2f, avgDrop,%3.2f,'
            'avgEvp,%3.2f, step,%3.2f, upperStepLimit,%3.2f,'
            'Period,%3.2f, pCalc,%3.2f, measP,%3.2f, Date,%s'
            %(self.electList,time.time(),self.setPoint,self.elecImp,
              self.temp,self.diff,self.rate,self.avgDrop.out,
              self.avgEvpRate.out,self.src,self.upperLimit,
              self.Period, self.pCalc,self.measP,time.ctime()))
        return tele

    def threadExit(self):
        if self.pqman != False:
            self.teleUpdate(str(self.name)+": "+"Pop & release")
            self.pqman.queue.pop(0)[0]
            self.pqman.lock.release()
            
    def trigger(self):
        self.process()

    def process(self):
        
            time.sleep(0.2)
            if self.triggerState == False:
                self.pqman.lock.acquire()

            ###Main###
            s = ('\nEnter: Period %3.2f'%(self.Period))
            #self.udp_send(s)
            self.teleUpdate(str(self.name)+": "+s)
            
            ###Time calculations###
            self.timeCalc()
            
            self.meas()
            if self.saveMeasImgFunc != False:
                self.saveMeasImgFunc()
                
                
            #--- Algorithm ---#
                
            ###Calculate rate###
            self.rateCalc()
                
            if self.disp == True:
                self.dispNum += 1
                
                ###Detect droplet at electrode###
                self.dispVerify()
                self.disp = False
                
                           
            else:
                ###Set rate as an evaporation###
                if self.rate < 0 and self.rate > -5:
                    self.avgEvpRate.step(self.rate)
                    
            #Count seconds since last syringe disp        
            if self.syrDisp == True:
                self.syrDispTimer = self.measP
                self.syrDisp = False ####reset to false####
            else:
                self.syrDispTimer += self.measP
                
            if self.syringe != True: ###EXPERIMENTAL
            
                #calsculate threashold (keeps average on the setpoint)    
                self.threshold = self.setPoint - (self.avgDrop.out/2.0)
                #Calculate error
                self.error = self.threshold - self.elecImp

            #average volume 
            self.avgVol.step(self.elecImp)

            tele = self.tele()
            tele = str(tele)
            
            #print(teleUpdate)
            #self.udp_send(tele)
            self.teleUpdate(tele)
            
            csv = open('electrode_volume_Measurements/NEWelecVol#%s.csv'%(self.electList), 'a')
##            csvWeb = open('C:/1/teleLOG.csv', 'a')
            csv.write(tele)
##            csvWeb.write(teleUpdate)
            csv.close()
##            csvWeb.close()

            ### moving graph ###
            #graph()
            
            #Calculate new period###
            self.calcPeriod()
                
            #Determine temperature
            if self.PID != False:
                self.temp = self.PID.PID.feedback[0]
                
            #Manage water level at the syringe reservoir
            if (self.syringe == True):
                ####eliminate excess water from overfilled###  
                self.resOverflow()
                ###set src value using PID###
                self.calcSyrStep()
            else:
                pass

            
            #Decide whether to dispense now or next cycle
            errorNow = -self.error
            errorNext = -(self.error -self.avgEvpRate.out*self.Period)
            if (errorNow > 0) and (errorNext > 0):
                s = ('Disp later')
                #self.udp_send(s)
                self.teleUpdate
                    
            else:
                self.disp = False
                if (abs(errorNow) < abs(errorNext)):
                    self.disp = True
                    if self.simMode == False:
                        
                        if (self.syringe == False) and (self.elecImp < (self.setPoint + 200)):
                            self.dispWait=(self.dispFunc(self.src, self.ori, self.des, self.orif))
                            if self.dispWait == 0:
                                if self.pqman != False:
                                    
                                    s = ('disp function returned 0')
                                    #self.udp_send(s)
                                    self.teleUpdate(str(self.name)+": "+s)
                                    self.pqman.lock.release()
                                    #self.threadExit()
                                    return
                                
                            time.sleep(float(self.dispWait)+0.1) #### experimental 
                            if self.saveDispImgFunc != False:
                                self.saveDispImgFunc()

                        elif (self.syrDispTimer >= self.syrDispTime):
                            s=('SYRINGE PUMPPING *********** setPoint is %s and elecImp is %s'%(self.setPoint, self.elecImp))
                            #self.udp_send(s)
                            self.teleUpdate(str(self.name)+": "+s)
                            time.sleep((self.dispFunc(self.src, self.ori, self.des, self.orif))+0.1) #### experimental 
                            self.syrDisp = True
                        else:
                            s=('Syringe disp must wait atleast: %3.2f seconds'%(self.syrDispTime-self.syrDispTimer))
                            #self.udp_send(s)
                            self.teleUpdate(str(self.name)+": "+s)
                            
                            self.teleUpdate('syringe wait')
                            self.pqman.lock.release()
                            #self.threadExit()
                            #if (self.syringe == False): ##experimental 
                                #res.trigger() ##experimental 
                             
                            return
                        
                    else:
                        self.drop.out()
                        
                    s = 'Disp now' 
                    #self.udp_send(s)
                    self.teleUpdate(str(self.name)+": "+s)
                    
                    s = str(abs(errorNow))
                    #self.udp_send(s)
                    self.teleUpdate(str(self.name)+": "+s)
                    
                    s = str(abs(errorNext))
                    #self.udp_send(s)
                    self.teleUpdate(str(self.name)+": "+s)
                    
      
            self.z1 = self.elecImp
            self.triggerState == False
            
            if (self.disp == True):
                #self.disp = False
                self.triggerState = True
                
                s = 'RETURN: self-triggered'
                #self.udp_send(s)
                self.teleUpdate(str(self.name)+": "+s)
                
                self.trigger()
                return
            
                if self.syringe == False:
                    time.sleep(0.2)
                    if self.redo != 0:
                        s = ( '############ redo =%s'%(self.redo))
                        #self.UDP_send(s)
                        self.teleUpdate(str(self.name)+": "+s)
                        self.redo-=1
                        
                        s = 'RETURN: out of redos'
                        #self.UDP_send(s)
                        self.teleUpdate(str(self.name)+": "+s)
                        self.trigger()
                        return

            if self.pqman != False:
                
                if self.syringe != True:
                    print('ok mix time')
                    time.sleep(self.mixFunc(self.des,self.orif))
                    
                s = 'RETURN: end of process'
                
                #self.UDP_send(s)
                self.teleUpdate(str(self.name)+": "+s)
                
                self.pqman.lock.release()
                #self.threadExit()
                return
                




##UDP1 = client.UDP_client(IP = '127.0.0.1', port = 8820)
##UDP2 = client.UDP_client(IP = '127.0.0.1', port = 8830)
##
##PQ = RM.priorityQueue(UDP_send = UDP1.send)
##
##sim2=electVol(15,simMode=True,adaptPeriod=False,pqman=PQ,nameID='sim2',UDP_send = UDP2.send)
##sim=electVol(11,simMode=True,adaptPeriod=False,pqman=PQ,nameID='sim',UDP_send = UDP2.send)
##PQ.objList=[sim, sim2]  

