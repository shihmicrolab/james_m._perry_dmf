#!/usr/bin/env python
'''
python 3 convert date: 2/March/2020
'''
import time

class DispCheck():
    '''
Class containing method to adjust actuation voltage for precise dispencing volume
    '''
    def __init__(self,
                 impScan=False,
                 dispFind=False,
                 dispExec=False,
                 voltage=False,
                 dropSize = 15.0,
                 boneSize = 5.0,
                 dropTol = 10.0,
                 voltsMax = 3.0,
                 voltsMin = 1.8,
                 Vpp = 2.0,
                 UDP_send = False
                 ):
        
        self.impScan = impScan
        self.dispFind = dispFind
        self.dispExec = dispExec
        self.voltage = voltage
        self.dropSize = dropSize
        self.boneSize = boneSize
        self.dropTol = dropTol
        self.voltsMax = voltsMax
        self.voltsMin = voltsMin
        self.Vpp = Vpp
        self.UDP_send = UDP_send

    def dropCalib(self,elec):
        self.dropSize = self.impScan(elec,3,self.Vpp)[0]
        print('dropSize calibrated to %3.2f'%(self.dropSize))
        self.UDP_send('dropSize calibrated to %3.2f'%(self.dropSize))

    def dispCheck(self,block,origin,period):
        period = 0.7
        drop = 0
        volts=self.voltage.initVolts
        t=time.time()
        bone = self.impScan(self.dispFind(origin,block)[0],self.Vpp)[0]

        if bone < self.boneSize:
            self.UDP_send('nothing to dispense')
            return 0
        self.voltage.setFunc(volts=(self.voltage.dispVolts))
        time.sleep(self.dispExec(block,origin,period)*1.1)
        drop = self.impScan(self.dispFind(origin,block)[1],3,2)[0]
        self.UDP_send('drop size is %3.2f'%drop) ###print out
        
        while drop < (self.dropSize - self.dropTol):
                self.voltage.dispVolts+=0.2
                if self.voltage.dispVolts > self.voltsMax:
                    self.voltage.dispVolts=self.voltsMax
                    self.UDP_send('voltage max reached: %3.2f Vpp ###### TRY SOMETHING ELSE'%(self.voltsMax))
                self.voltage.setFunc(volts=(self.voltage.dispVolts))
                time.sleep(self.dispExec(block,origin,period)*1.1)
                drop = self.impScan(self.dispFind(origin,block)[1],3,self.Vpp)[0]
                self.UDP_send('drop size is %3.2f'%drop) ###print out
                period += 0.1
                if period > 0.8:
                    self.UDP_send('dispense Error')
                    return 0
                
        while drop > (self.dropSize + self.dropTol):
                self.voltage.dispVolts-=0.2
                self.voltage.setFunc(volts=(self.voltage.dispVolts))
                if self.voltage.dispVolts < self.voltsMin:
                    self.voltage.dispVolts= self.voltsMin
                    self.UDP_send('voltage min reached: %3.2f Vpp ###### TRY SOMETHING ELSE'%(self.voltsMin))
                time.sleep(self.dispExec(block,origin,period)*1.1)
                drop = self.impScan(self.dispFind(origin,block)[1],3,2)[0]
                self.UDP_send('drop size is %3.2f'%drop) ###print out
                period -= 0.1
                if period < 0.2:
                    self.UDP_send('dispense Error')
                    return 0
                
        return time.time()-t
            
        
        
    
                 
