import sys
# https://pyvisa.readthedocs.io/en/master/tutorial.html
'''
python 3 convert date: 2/March/2020
'''
import pyvisa as visa
class setFunc():
    def __init__(self,func='SIN',
                 freq='15000',
                 volt='2.0'):
        self.func=func,
        self.freq=freq,
        self.volt=volt
        
##    def setFunc(self):
##        fg.write('APPL:%s %s,%s'%(self.func, self.freq, self.volt)
##        

    
strHelp = '\nUse SIN/SQU frequency voltage e.g.\nsetFunc.py sin 15000 2.2'
"""Initialize our function generator, connected via usb"""
rm = visa.ResourceManager()
fg_id = (rm.list_resources())[0]
print('Using device: %s'%(fg_id))
fg = rm.open_resource(fg_id)

##fg.write('APPL:SIN freq, Volt')
##
##fg.query('*IDN?') #Like write and read
##
##fg.write('*IDN?') #Same as quary
##fg.read() #

arg1 = sys.argv[0]
if (arg1 == 'h') or (arg1 == 'help'):
    print('VISA devices:\n%s'%(rm.list_resources()))
    print(strHelp)

elif (arg1 == 'ON') or (arg1 == 'on') or (arg1 == '1'):
    #Turn the output of the function generator on
    fg.write("OUTP ON")

elif (arg1 == 'OFF') or (arg1 == 'off') or (arg1 == '0'):
    #Turn the output of the function generator off
    fg.write("OUTP OFF")

elif (arg1 == 'SIN') or (arg1 == 'sin') or (arg1 == 'SQU') or (arg1 == 'squ'):
    #Turn the output of the function generator on
    fg.write("OUTP ON")

    #Apply a sine or square wave with given frequency and amplitude.
    fg.write('APPL:%s %s,%s'%(sys.argv[1], sys.argv[2], sys.argv[3]))
else:
    print('wrong argument!\n%s'%(strHelp))


'''
#set the func gen 
fg.write('APPL:SIN freq, Volt')

#set number of cycles
fg.write('BURS:NCYC 8')

#set mode to trigger
fg.write('BURS:MODE TRIG')

#set to usb trigger
fg.write('TRIG:SOUR BUS')

# Turn on burst
fg.write('BURS:STAT ON')

#trigger switch
fg.write('*TRG')

'''

def initBurst(amp=0.02,freq=15000,cycles=8,offset=0.01):
    fg.write('APPL:SQU %3.2f, %3.2f'%(amp,freq))
    fg.write('VOLT:OFFS %3.2'%(offset))
    fg.write('BURS:NCYC %d'%(cycles))
    fg.write('BURS:MODE TRIG')
    fg.write('TRIG:SOUR BUS')
    fg.write('BURS:STAT ON')

def burst():
    fg.write('*TRG')
    
