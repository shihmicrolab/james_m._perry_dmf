"""
By: Guy Soffer & James Perry
Date: 16/Aug/2019
python 3 convert date: 2/March/2020
"""

from GSOF_ArduBridge import threadBasic as BT
from GSOF_ArduBridge import UDP_Send
from GSOF_ArduBridge import udpControl

class counter():
    def __init__(self):
        self.cnt = 0

    def inc(self):
        self.cnt += 1

class ExtFileThread(BT.BasicThread):
    """
    """
    def __init__(self, nameID='extCmdFile', Period=2.0, cmdFile='extCmd.txt', viewer={}):
        """
        """
        #super(StoppableThread, self).__init__()
        BT.BasicThread.__init__(self, nameID=nameID, Period=Period, viewer=viewer)
        self.cmdFile = cmdFile #The external file that contain the code for executation
        self.cmdFileOld = cmdFile+'.old'   


    def process(self):
        """
        Your process code should go here.
        """
        try:
            f = open(self.cmdFile, 'r')
        except IOError:
            f = False

        if f != False:
            try:
                s = f.read()
                s = str(s)
                extEval(s)
                print('File executed')
            except SyntaxError:
                print('File error')
            f.close()
            if os.path.isfile(self.cmdFileOld):
                os.remove(self.cmdFileOld)

            renameDone = False
            while renameDone == False:
                try:
                    os.rename(self.cmdFile, self.cmdFileOld)
                    renameDone = True
                except WindowsError as e:
                    print(str(e))
                    print('Cannot rename the file')
                    continue
                
udpSendCam = UDP_Send.udpSend(nameID='CHIP', DesIP='127.0.0.1', DesPort=7003)    
