class seqList():
    def __init__(self):
        
        self.t=[]
        #1 Tracking: these are the sequences for each section of 'tracking electrodes'
        self.t.append([0]) #Empty space so self.t[0] is empty        
        self.t.append( 	[67, 14, 15, 69, 16, 70]    )
        self.t.append( 	[71, 18, 19, 73, 20, 74]    )
        self.t.append( 	[75, 22, 23, 78, 24, 79]    )
        self.t.append( 	[0] )
        self.t.append( 	[0] )
        self.t.append( 	[0] )
        self.t.append( 	[0] )
        self.t.append( 	[0] )
        self.t.append([0]) #Empty space
        self.t.append([0]) #Empty space

        #11 Dispensing from the top
        self.t.append( 	[12, 13, 67,12]	)
        self.t.append( 	[16, 17, 71,16]	)
        self.t.append( 	[20, 21, 75, 20] )
        self.t.append( 	[0]	)
        self.t.append( 	[0]	)
        self.t.append( 	[0]	)
        self.t.append( 	[0]	)
        self.t.append( 	[0]	)
        self.t.append( 	[0]	)
        self.t.append([0]) #Empty space
        
        #21 Dispensing from the bottom
        self.t.append( 	[62, 85, 6, 62]	)
        self.t.append( 	[64, 87, 8, 64]	)
        self.t.append( 	[66, 89, 10, 66]	)
        self.t.append( 	[ 91, 12, 68]	)
        self.t.append( 	[93, 14, 70]	)
        self.t.append( 	[95, 16, 72]	)
        self.t.append( 	[74, 97, 18, 74]	)
        self.t.append( 	[76, 99, 20, 76]	)
        self.t.append( 	[78, 101, 22, 78]	)

        self.t.append([0]) #Empty space
        #31 Delivering from the top
        self.t.append( 	[6, 29, 5, 6]	)
        self.t.append( 	[8, 31, 7 , 8]	)
        self.t.append( 	[10, 33, 9, 10]	)
        self.t.append( 	[13, 35, 11, 13]	)
        self.t.append( 	[14, 37, 13 , 14]	)
        self.t.append( 	[16, 39, 15, 16]	)
        self.t.append( 	[18, 41, 17, 18]	)
        self.t.append( 	[20, 43, 19, 19]	)
        self.t.append( 	[22, 45, 21, 22]	)

        self.t.append([0]) #Empty space
        #41 Delivering from the bottom
        self.t.append( 	[85, 62]	)
        self.t.append( 	[8, 63,[ 8, 63, 87], 87, 64]	)
        self.t.append( 	[10, 65,[ 10, 65, 89], 89, 66]	)
        self.t.append( 	[12, 67,[ 12, 67, 91], 91, 68]	)
        self.t.append( 	[14, 69,[ 14, 69, 93], 93, 70]	)
        self.t.append( 	[16, 71, [16, 71, 95], 95, 72]	)
        self.t.append( 	[18, 73, [18, 73, 97], 97, 74]	)
        self.t.append( 	[20, 75,[20, 75, 99], 99, 76]	)
        self.t.append( 	[22, 77, [22, 77, 101], 101, 78]	)

        self.t.append([0]) #Empty space
        #51 mix top
        self.t.append( 	[5, 29, 61, 29, 5, 29]	)
        self.t.append( 	[7, 31, 63, 31, 7, 31]	)
        self.t.append( 	[9, 33, 65, 33, 9, 33]	)
        self.t.append( 	[11, 35, 67, 35, 11, 35]	)
        self.t.append( 	[13, 37, 69, 37, 13, 37]	)
        self.t.append( 	[15, 39, 71, 39, 15, 39]	)
        self.t.append( 	[17, 41, 73,73, 41, 17,17 ]	)
        self.t.append( 	[19, 43, 75,75, 43, 19,19 ]	)
        self.t.append( 	[21, 45, 77,77, 45, 21,21 ]	)


        self.t.append([0]) #Empty space
        #51 mix bottom
        self.t.append( 	[62, 85, 6, 85, 62, 85]	)
        self.t.append( 	[64, 87, 8, 87, 64, 87]	)
        self.t.append( 	[66, 89, 10, 89, 66, 89]	)
        self.t.append( 	[68, 91, 13, 91, 68, 91]	)
        self.t.append( 	[70, 93, 14, 93, 70, 93]	)
        self.t.append( 	[72, 95, 16, 95, 72, 95]	)
        self.t.append( 	[74, 97, 18, 97, 74, 97]	)
        self.t.append( 	[76, 99, 20, 99, 76, 99]	)
        self.t.append( 	[78, 101, 22, 101, 78, 101]	)
        
