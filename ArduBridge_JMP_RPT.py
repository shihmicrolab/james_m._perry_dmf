 
#!/usr/bin/env python
"""
Script to build an ArduBridge environment
To customize the environment to your needs. You will need to change
the parameters in the "PARAMETER BLOCK" in the __main__ section

By: Guy Soffer and James Perry
Date: 08/May/2018
"""

import time, os
from matplotlib import pyplot as plt
import threading
import subprocess

from GSOF_ArduBridge_py2 import udpControl
from GSOF_ArduBridge_py2 import ArduBridge
from GSOF_ArduBridge_py2 import ArduBridge_HW
from GSOF_ArduBridge_py2 import ElectrodeGpioStack
from GSOF_ArduBridge_py2 import threadPID_HW11 as threadPID
from GSOF_ArduBridge_py2 import UDP_Send
from GSOF_ArduBridge_py2 import threadBasic as BT

print 'this'

from JMP_MOD_lib_py2 import stepperMotor
from JMP_MOD_lib_py2 import movAvg
from JMP_MOD_lib_py2 import thermalCycle
from JMP_MOD_lib_py2 import electVol_6 as electVol
from JMP_MOD_lib_py2 import electVol_mon
print 'this'
from JMP_MOD_lib_py2 import setFuncClass
print 'this'
from JMP_MOD_lib_py2 import impScan as IS
from JMP_MOD_lib_py2 import client
from JMP_MOD_lib_py2 import dispCheck as DC
from JMP_MOD_lib_py2 import replenishManager as RM

print 'this'

class counter():
    def __init__(self):
        self.cnt = 0

    def inc(self):
        self.cnt += 1

class ExtFileThread(BT.BasicThread):
    """
    """
    def __init__(self, nameID='extCmdFile', Period=2.0, cmdFile='extCmd.txt', viewer={}):
        """
        """
        #super(StoppableThread, self).__init__()
        BT.BasicThread.__init__(self, nameID=nameID, Period=Period, viewer=viewer)
        self.cmdFile = cmdFile #The external file that contain the code for executation
        self.cmdFileOld = cmdFile+'.old'

    def process(self):
        """
        Your process code should go here.
        """
        try:
            f = open(self.cmdFile, 'r')
        except IOError:
            f = False

        if f != False:
            try:
                s = f.read()
                s = str(s)
                extEval(s)
                print 'File executed'
            except SyntaxError:
                print 'File error'
            f.close()
            if os.path.isfile(self.cmdFileOld):
                os.remove(self.cmdFileOld)

            renameDone = False
            while renameDone == False:
                try:
                    os.rename(self.cmdFile, self.cmdFileOld)
                    renameDone = True
                except WindowsError as e:
                    print str(e)
                    print 'Cannot rename the file'
                    continue
    
#Execute string as python code
def extEval(s):
    try:
        print s
        s = str(s)
        exec(s)
    except SyntaxError:
        print 'Command error: %s'%(s)

#Execute python code located in external file

def close():
    if udpConsol != False:
        udpConsol.active = False
    if fileConsol != False:
        fileConsol.active = False
    setup.stop()

def saveDispSeq():
    udpSendCam.Send("for i in range(0,10,1):\n  DVR.saveImage('c:/1/dispanse_seq_%d.jpg'%(i))\n  time.sleep(1)")

def saveFlipSeq():
    udpSendCam.Send("for i in range(0,10,1):\n  DVR.saveImage('c:/1/flip_seq_%d.jpg'%(i))\n  time.sleep(1)")

def saveChipImg():
    fileName = 'c:/1/chip_seq_%d.jpg'%(chipImgIdx.cnt)
    command = "DVR.saveImage('%s')"%(fileName)
    udpSendCam.Send(command)
    chipImgIdx.inc()

    fileName = 'c:/1/chip_seq.jpg'
    command = "DVR.saveImage('%s')"%(fileName)
    udpSendCam.Send(command)
    
if __name__ == "__main__":
    chipImgIdx = counter()
    #\/\/\/ CHANGE THESE PARAMETERS \/\/\/
    import Protocol_JMP_RPT as protocol #<--Your experiment protocol file name
    port = 'COM3' #<--Change to the correct COM-Port to access the Arduino
    #baudRate = 115200 #<--Leave as is
    baudRate = 115200*2 #<--Leave as is
    ONLINE = True#False#False #<--True to enable work with real Arduino, False for simulation only.
    PID1 = True#False #<--True / False to build a PID controller.
    PID2 = True #False #<--True / False to build a PID controller.
    ELEC_EN = True #<--True to enable the real electrodes, False for simulation only.
    STACK_BUILD = [0x40,0x41,0x42,0x43,0x44,0x45]
    PORT_BASE = 8000 #7000
    REMOTE_CTRL_PORT = PORT_BASE +10 #-1
    REMOTE_FILE_NAME = 'c:/1/extCmdFile.txt'
    #/\/\/\   PARAMETERS BLOCK END  /\/\/\

    udpSendPid1 = UDP_Send.udpSend(nameID='PID1', DesIP='127.0.0.1', DesPort=6100)
    udpSendPid2 = UDP_Send.udpSend(nameID='PID2', DesIP='127.0.0.1', DesPort=6102)
    udpSendChip = UDP_Send.udpSend(nameID='CHIP', DesIP='127.0.0.1', DesPort=8020)
    udpSendCam = UDP_Send.udpSend(nameID='CHIP', DesIP='127.0.0.1', DesPort=7003)


    #JMP's UDP viewer system
    subprocess.Popen(['C:/James_M._Perry_DMF/volume_viewer.bat'],shell=False)

    UDP1 = client.UDP_client(IP = '127.0.0.1', port = 8820)
    UDP2 = client.UDP_client(IP = '127.0.0.1', port = 8830)
    UDP3 = client.UDP_client(IP = '127.0.0.1', port = 8840)
    
    if REMOTE_FILE_NAME != '':
        fileConsol = ExtFileThread(nameID='extCmdFile', Period=2.0, cmdFile=REMOTE_FILE_NAME)
        fileConsol.start()

    if REMOTE_CTRL_PORT > 1:
        udpConsol = udpControl.udpControl(nameID='udpIDLE', RxPort=REMOTE_CTRL_PORT, callFunc=extEval)
        print 'Remote-Consol-Active on port %s\n'%(str(REMOTE_CTRL_PORT))
    print 'Using port %s at %d'%(port, baudRate)
    ardu = ArduBridge.ArduBridge( COM=port, baud=baudRate )
    hw = ArduBridge_HW.ArduBridge_Shield(ardu)
    if ONLINE:
        ardu.OpenClosePort(1)
        print 'Connecting to Arduino ON-LINE.'
    else:
        print 'Arduino OFF-LINE. Simulation mode'

    ExtGpio = ElectrodeGpioStack.ExtGpioStack(i2c=ardu.i2c, devList=STACK_BUILD, v=False)#True)
    ExtGpio.init()
    ExtGpio.init()
    ardu.Reset()
    print 'Ready...\n'

    if PID1 == True:
        PID = threadPID.ArduPidThread(bridge=ardu,
                                      nameID='PID1',       # Process name
                                      Period=0.5,          # The PID calculation cycle-time
                                      fbPin=0,             # The analog pin of the temperture sensor
                                      outFunc=hw.pwmB,     # The PWM output to the H-Bridge
                                      #outFunc=hw.pwmA,     # The PWM output to the H-Bridge
                                      viewer={'UDP1': udpSendPid1.Send}
                                      )
        PID.filterN = 1
        PID.PID.Kp = 7.0
        PID.PID.Ki = 1.0
        PID.PID.Kd = 0.0
        PID.RC_div_DT=0.5
        PID.enOut = True
        hw.ssrA(1)
        hw.pwmB_init()
        hw.pwmA_init()
        print 'type PID.start() to start the PID thread\n'

    if PID2 == True:
        PID2 = threadPID.ArduPidThread(bridge=ardu,
                                      nameID='PID2',
                                      Period=0.5,          # The PID calculation cycle-time
                                      fbPin=1,             # The analog pin of the temperture sensor
                                      outFunc=hw.pwmA,     # The PWM output to the H-Bridge
                                      viewer={'UDP2': udpSendPid2.Send}
                                      )
        PID2.PID.Kp = 7.0
        PID2.PID.Ki = 1.0
        PID2.PID.Kd = 0.0
        PID.RC_div_DT=0.5
        PID2.enOut = True
        hw.pwmB_init()
        hw.ssrB(1)
        print 'type PID.start() to start the PID thread\n'

    setup = protocol.Setup(ExtGpio=ExtGpio, gpio=ardu.gpio, chipViewer=udpSendChip.Send, UDP_send = UDP3.send)
    setup.enOut(ELEC_EN)


def DNA(b=4,o=0,d=9,of=0,N=1,M=1,p=0.5):
    period = p#[sec]
    for Si in range(0,N):
        for Di in range(0,M):
            SrcBlock = b-Si
            DesBlock = d-Di
            elecList = setup.disp(o,SrcBlock)[0:-1] +setup.autoTrack(SrcBlock ,DesBlock)[0:-1] +setup.deli(of,DesBlock)
            t = (len(setup.disp(o,SrcBlock)) +1)*period
            setup.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=period
                            )
            print t
            if Di<M-1:
                time.sleep(t)
                
#########Droplet Actuation HOTKEYS########
                
def disp(b,o):
    setup.dispExec(b,o)

def track(bgBlock, endBlock,p=0.5):
    setup.trackExec(bgBlock, endBlock,p=0.5)
    
def deli(blk,ori,p=0.5):
    setup.deliExec(ori, blk,p=0.5)
    
def mix(blk=9,ori=0,of=0,N=20,p=0.5):
    setup.mixExec(ori, blk,p=p,n=N)
        
def distro(b,o,d,of,p=0.5):
    setup.distro(b,o,d,of,p)

def flip(b,o,p=0.5):
    setup.flip(b,o,p=0.5)

def dispCheck(block, origin, period=0.3):
    '''This method is just a local short-cut. DCO represents the
    dispCheck object used to hold the disp check method and
    parameters'''
    return DCO.dispCheck(block, origin, period)
    
##def dispCheck(b,o,p=0.3,dropSize=60,vLim=3.0):
##    drop = 0
##    volts=voltage.initVolts
##    t=time.time()
##    bone = impScan(setup.dispFind(o,b)[0],Vpp=2)[0]
##
##    if bone < 5:
##        print 'nothing to dispense'
##        return 0
##    voltage.setFunc(volts=(voltage.dispVolts))
##    time.sleep(setup.dispExec(b,o,p)*1.1)
##    drop = impScan(setup.dispFind(o,b)[1],3,2)[0]
##    while drop < (dropSize-5):
##            voltage.dispVolts+=0.2
##            if voltage.dispVolts > vLim:
##                voltage.dispVolts=vLim
##                print 'voltage limit reached: %3.2f Vpp ###### TRY SOMETHING ELSE'%(vLim)
##            voltage.setFunc(volts=(voltage.dispVolts))
##            time.sleep(setup.dispExec(b,o,p)*1.1)
##            drop = impScan(setup.dispFind(o,b)[1],3,2)[0]
##            p += 0.2
##            if p > 0.8:
##                print 'dispense Error'
##                return 0
##    while drop > (dropSize+5):
##            voltage.dispVolts-=0.2
##            voltage.setFunc(volts=(voltage.dispVolts))
##            time.sleep(setup.dispExec(b,o,p)*1.1)
##            drop = impScan(setup.dispFind(o,b)[1],3,2)[0]   
##            p -= 0.2
##            if p < -0.6:
##                print 'dispense Error'
##                return 0
##    return time.time()-t

def dex(b,o,d,of,p1=0.3,p2=0.1):
    #setup.dispExec(o,b,p1)
    #time = len(setup.dispFind(o,b))*p1
    time=dispCheck(b,o,p1)
    if time == 0:
        return time
    timeb = (len(setup.trackFind(b,d))*p2)
    timec = (len(setup.deliFind(b,d))*p1)
    t1=threading.Timer(0,setup.trackExec,[b,d,p2])
    t2=threading.Timer(timeb,setup.deliExec,[of,d,p1])
    t1.start()
    t2.start()
    return time + timeb + timec

def multi(func=dex,source=[],dest=[],pause=3):
	i=0
	param=[]
	for n in source:
	#for m in dest:
		for m in dest:
		#for n in source:
			param.append(n+m)
	for args in param:
		threading.Timer(pause*i,func,args).start()
		i+=1


##########################################    

    
def stop():
    setup.stop()
    
#setup.enOut()

def fan(fanPin=12):
    status=ardu.gpio.digitalRead(fanPin)
    if status == 0:
        ardu.gpio.digitalWrite(fanPin,1)
    else:
        ardu.gpio.digitalWrite(fanPin,0)


def hs(RC=0.5,T=37,t=30):
       
	PID.RC_div_DT=RC
	PID.ctrl(T)
	time.sleep(t)
	PID.RC_div_DT=0.5
	PID.ctrl(0)
	time.sleep(3*60)
	PID.RC_div_DT=22
	PID.ctrl(24)
	
	

def impScan(elects, retry=3, Vpp=1):
    imp = []
    if type(elects) != 'int':
        elects = [elects]
    for elect in elects:
        ExtGpio.pinWrite(elect, 1)
        time.sleep(.5)
        voltage.setFunc(volts=Vpp)
        time.sleep(.5)
        val = -1
        while (retry > 0) and ((val > 1023) or (val < 0)):
            val = ardu.an.analogRead(1)
            UDP3.send( 'invalid impScan val: Retry')
            retry -= 1
                
        imp.append(val)
        UDP3.send( 'elec# %d, %d'%(elect, val))
        voltage.setFunc(volts=voltage.initVolts)
        ExtGpio.pinWrite(elect, 0)
        
    return imp

def syringe(movRel=0,a=0,b=0,c=0):
    stepper.moveRel(movRel)
    return 1

def stepVol(forward=-5,back=0,elec=64,n=30,period=1, repeats=1):
    T0 = time.time()
    data = []
    t = []
    Time = 0
    for r in range(repeats):
        stepper.moveRel(forward)
        stepper.moveRel(back)
        for i in range(n):
            smpTime = time.time() -T0
            data.append(impScan(elec))
            t.append(smpTime)
            Time = Time +period
            while (Time -(time.time() -T0)) > 0:
                time.sleep(0.05)

    plt.figure()
    plt.xlabel('Time (sec)')
    plt.ylabel('Impedance')
    plt.plot(t, data)
    plt.show()

voltage=setFuncClass.setFunc(initVolts=2.2,dispVolts=2.5, UDP_send = UDP3.send)
#impScan = IS.impScan(ExtGpio=ExtGpio,ardu=ardu,setFunc=setFuncClass.setFunc())

stepper = stepperMotor.stepperMotor(ardu.gpio, pinMap=[8,9,10,11])
stepper.config()

DCO = DC.DispCheck(
                        impScan=impScan,
                        dispFind=setup.dispFind,
                        dispExec=setup.dispExec,
                        voltage=voltage,
                        dropSize = 40.0,
                        dropTol = 3.0,
                        voltageLimit = 3.0,
                        Vpp = 2.0,
                        UDP_send = UDP3.send
                        )

##############Volume replenishment objects###################

def pulse(n=100,t=0.01):
    for i in range(n):
        ardu.gpio.digitalWrite(3,1)
        time.sleep(t)
        ardu.gpio.digitalWrite(3,0)
        time.sleep(t)


PQ = RM.priorityQueue(UDP_send = UDP2.send)

res=electVol.electVol(
                 electList = 72,
                 impScan = impScan,
                 dispFunc = syringe,src=-4, ori=0, des=0, orif=0,
                 flipFunc = dex,flipArg=[6,1,6,0],
                 Period = 60,
                 setPoint = 300,
                 nameID='res (elec 72)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 syringe = True,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc=electVol.electVol(
                 electList = 17,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=7, orif=0,
                 Period = 60,
                 setPoint = 205,
                 nameID='inc (elec 17)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc1=electVol.electVol(
                 electList = 19,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=8, orif=0,
                 Period = 60,
                 setPoint = 205,
                 nameID='inc1 (elec 19)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc2=electVol.electVol(
                 electList = 21,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=9, orif=0,
                 Period = 60,
                 setPoint = 193,
                 nameID='inc2 (elec 21)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc3=electVol.electVol(
                 electList = 74,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=7, orif=1,
                 Period = 60,
                 setPoint = 89,
                 nameID='inc4 (elec 74)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc4=electVol.electVol(
                 electList = 76,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=8, orif=1,
                 Period = 60,
                 setPoint = 154,
                 nameID='inc4 (elec 76)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

inc5=electVol.electVol(
                 electList = 78,
                 impScan = impScan,
                 dispFunc = dex,src=6, ori=1, des=9, orif=1,
                 Period = 60,
                 setPoint = 232,
                 nameID='inc5 (elec 78)',
                 UDP_send = UDP1.send,
                 adaptPeriod = False,
                 pqman=PQ,
                 simMode = False,
                 PID = PID,
                 saveDispImgFunc = saveDispSeq,
                 saveMeasImgFunc = saveChipImg,
                 saveFlipImgFunc = saveFlipSeq,
                 redoN=2,
                 )

samples = [res,inc,inc1,inc2,inc3,inc4,inc5]

PQ.objList = samples



###############Thermal Cycler #############

tempList = []
tempList.append(thermalCycle.thermalPoint(37,60*10))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(37,60*1.15))
tempList.append(thermalCycle.thermalPoint(16,60*3))

tempList.append(thermalCycle.thermalPoint(10,60*20))



moclo = thermalCycle.thermoCycler(pid=PID,pntList=tempList)
mocloTrigger = thermalCycle.thermoCycler(pid=PID,pntList=tempList,sideChain=inc.trigger,sideChainTempChange=inc.tempChange)

ardu.gpio.digitalWrite(12,0)
#ExtGpio.pinWrite(66, 0)
print 'FINISHED INITIALIZATION'
