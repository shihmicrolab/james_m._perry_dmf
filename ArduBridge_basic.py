#!/usr/bin/env python
"""
Script to build an ArduBridge environment
To customize the environment to your needs. You will need to change
he parameters in the "PARAMETER BLOCK" in the __main__ section

By: Guy Soffer and James Perry
Date: 11/July/2018
"""

#Basic modules to load
import time
from GSOF_ArduBridge import udpControl
from GSOF_ArduBridge import ArduBridge
from GSOF_ArduBridge import ElectrodeGpioStack
from GSOF_ArduBridge import threadPID
from GSOF_ArduBridge import UDP_Send

def extEval(s):
    s=str(s)
    eval(s)

def close():
    if udpConsol != False:
        udpConsol.active = False
    setup.stop()
    ardu.OpenClosePort(0)

if __name__ == "__main__":
    #\/\/\/ CHANGE THESE PARAMETERS \/\/\/
    import Protocol_JMP_RPT as protocol #<--Your experiment protocol file name
    port = 'COM3' #<--Change to the correct COM-Port to access the Arduino
    baudRate = 115200*2 #<--ArduBridge_V1.0 uses 115200 other versions use 230400 = 115200*2 
    ONLINE = True #False #False #<--True to enable work with real Arduino, False for simulation only.
    PID1 = False #<--True / False to build a PID controller.
    ELEC_EN = False #<--True to enable the real electrodes, False for simulation only.
    STACK_BUILD = [0x40,0x41,0x42,0x43,0x44,0x45]
    REMOTE_CTRL_PORT = 7010
    #/\/\/\   PARAMETERS BLOCK END  /\/\/\
    
    udpSendPid = UDP_Send.udpSend(nameID='', DesIP='127.0.0.1', DesPort=6000)
    udpSendChip = UDP_Send.udpSend(nameID='', DesIP='127.0.0.1', DesPort=6001)
    udpConsol = False
    if REMOTE_CTRL_PORT > 1:
        udpConsol = udpControl.udpControl(nameID='udpIDLE', RxPort=REMOTE_CTRL_PORT, callFunc=extEval)
        print('Remote-Consol-Active on port %s\n'%(str(REMOTE_CTRL_PORT)))
    print('Using port %s at %d'%(port, baudRate))
    ardu = ArduBridge.ArduBridge( COM=port, baud=baudRate )
    if ONLINE:
        ardu.OpenClosePort(1)
        print('Connecting to Arduino ON-LINE.')
    else:
        print('Arduino OFF-LINE. Simulation mode')

    ExtGpio = ElectrodeGpioStack.ExtGpioStack(i2c=ardu.i2c, devList=STACK_BUILD, v=False)#True)
    ExtGpio.init()
    ExtGpio.init()
    ardu.Reset()
    print('Ready...\n')

    if PID1 == True:
        PID = threadPID.ArduPidThread(bridge=ardu,
                                      nameID='PID',
                                      Period=0.5,   #Period-time of the control-loop
                                      fbPin=1,      #The feedback pin (sensor)
                                      outPin=3,     #The output pin (driver)
                                      dirPin=9      #The direction pin for the H-bridge
                                      )
        PID.PID.Kp = 30
        PID.PID.Ki = 1.2
        PID.PID.Kd = 0.0
        PID.addViewer('UDP',udpSendPid.Send)
        PID.enIO(True)
        ardu.gpio.pinMode(9,0)
        print('type PID.start() to start the PID thread\n')

    setup = protocol.Setup(ExtGpio=ExtGpio, gpio=ardu.gpio, chipViewer=udpSendChip.Send)
    setup.enOut(ELEC_EN)
