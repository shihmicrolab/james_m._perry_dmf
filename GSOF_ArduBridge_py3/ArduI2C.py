#!/usr/bin/env python
from GSOF_ArduBridge import BridgeSerial
from GSOF_ArduBridge import CON_prn
import time

"""
Class to access the Arduino-Bridge I2C bus.
This class is using the BridgeSerial class object to communicate over serial
with the Arduino-Bridge firmware.
The packet has a binary byte based structure
To write:
<'2'>,<'A'>,<DEV#>,<'L'>,<n+1>,<REG#>,<DAT1>, <DAT2>...<DATn>

To read:
<'2'>,<'A'>,<DEV#>,<'L'>,<1>,<'w'>,<REG#>
<'2'>,<n>,<'R'>

note:'2' I2C packet header
     'w' write-restart
     'R' read
     'A' device-address
     'L' data lenght

By: Guy Soffer
Date: 20/Feb/2018
Python 3 convert Date: 2/March/2020
"""
class ArduBridgeI2C():
    def __init__(self, bridge=False, v=False):
        self.v = v
        self.comm = bridge
        self.RES = {1:'OK', -1:'ERR'}

        #I2C protocol command
        self.I2C_PACKET_ID = ord('2')
        self.CMD_I2C_ADDRESS = ord('A')
        self.CMD_I2C_LENGTH = ord('L')
        self.CMD_I2C_WRITE_RESTART = ord('w')
        self.CMD_I2C_WRITE = ord('W')
        self.CMD_I2C_READ_RESTART = ord('r')
        self.CMD_I2C_READ = ord('R')

        self.ERROR_NONE	= ord('N')
        self.ERROR_UNESCAPE = ord('U')
        self.ERROR_LENGTH = ord('L')
        self.ERROR_READ = ord('R')
        self.ERROR_WRITEDATA = ord('W')
        self.ERROR_SENDDATA = ord('S')

        self.ERROR = {self.ERROR_NONE:'OK',
                      self.ERROR_UNESCAPE:'UNESCAPE',
                      self.ERROR_LENGTH:'LENGTH',
                      self.ERROR_READ:'READ',
                      self.ERROR_WRITEDATA:'WRITE-DATA',
                      self.ERROR_SENDDATA:'SEND-DATA'}

    def writeRegister(self, dev, reg, vByte):
        vDat = [self.I2C_PACKET_ID,   #I2C packet-ID
                self.CMD_I2C_ADDRESS, #next byte is the I2C device-address
                dev,                  #DEV#
                self.CMD_I2C_LENGTH,  #Next byte is the data length (including the register#)
                len(vByte) +1,        #Data length
                self.CMD_I2C_WRITE,   #Next bytes should be sent as is to the I2C device
                reg] +vByte           #REG# +data-vector
        
        self.comm.send(vDat)
        reply = self.comm.receive(1)
  
        if self.v:
            if reply[0] != 0:      #did we received a byte
                res = reply[1][0]  #if yes, read the result
                CON_prn.printf('I2C-WR: Dev-0x%02x, Reg%d - %s', par=(dev, reg, self.ERROR[res]), v=True)
        return reply

    def readRegister(self, dev, reg, N):
        vHdr = [self.I2C_PACKET_ID,         #I2C packet-ID
                self.CMD_I2C_ADDRESS,       #next byte is the I2C device-address
                dev,                        #DEV#
                self.CMD_I2C_LENGTH,        #Next byte is the data length (including the register#)
                1,                          #Data length
                self.CMD_I2C_WRITE_RESTART, #Next bytes should be sent as is to the I2C device
                reg]

        vRd  = [self.I2C_PACKET_ID,         #I2C packet-ID
                self.CMD_I2C_LENGTH,        #Next byte is the data length (including the register#)
                N,                          #N bytes to read
                self.CMD_I2C_READ]#_RESTART]  #Start the read sequence

        self.comm.send(vHdr +vRd)
        reply = self.comm.receive(1)        #1st byte is the ACK for the register write
        if (reply[0] != 0) and (reply[0] != -1):
            reply = self.comm.receive(1)        #2nd byte is how many bytes where read
            if reply[0] != 0:
                n = reply[1][0]
                reply = self.comm.receive(n)    #Read n bytes
                if reply[0] != 0:
                    val = reply[1]
                    CON_prn.printf('I2C-RD: Dev-0x%02x, Reg%d, Dat %s ', par=(dev, reg, str(val)), v=self.v)
                    return val
        CON_prn.printf('I2C-RD: Dev%d, Reg%d - Error', par=(dev, reg), v=self.v)
        return -1
