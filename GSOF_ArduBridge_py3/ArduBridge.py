'''
Python 3 convert Date: 2/March/2020
'''
from GSOF_ArduBridge import BridgeSerial 
from GSOF_ArduBridge import ArduAnalog
from GSOF_ArduBridge import ArduGPIO 
from GSOF_ArduBridge import ArduI2C 

class ArduBridge():
    def __init__(self, COM='COM9', baud=115200):
        self.ExtGpio = [0,0]
        self.COM  = COM
        self.comm = BridgeSerial.ArduBridgeComm( COM=COM, baud=baud )
        self.gpio = ArduGPIO.ArduBridgeGPIO( bridge=self.comm )
        self.an   = ArduAnalog.ArduBridgeAn(bridge=self.comm )
        self.i2c  = ArduI2C.ArduBridgeI2C( bridge=self.comm )#, v=True )

    def OpenClosePort(self, val):
        if type(val) == str:
            if val == 'open':
                val = 1
            else:
                val = 0
        self.comm.OpenClosePort(val)
        if val != 0:
            self.GetID()

    def Reset(self):
        self.comm.sendReset()
        self.comm.uart_flush()
        self.GetID()

    def GetID(self):
        self.comm.send([ord('?')])
        reply = self.comm.receive(1)
        if reply[0] != -1:
            s = ''
            ACK = 1
            N = reply[1][0]
            while (ACK != -1) and (N > 0):
                reply = self.comm.receive(1)
                ACK = reply[0]
                if ACK != -1:
                    N -= 1
                    #print chr(reply[1][0])
                    s += chr(reply[1][0])
            s += '\n'
            print('%s'%(s))
#            if reply[0] == 1:
#                print 'Got reply\n'
        else:
            print('No reply\n')
