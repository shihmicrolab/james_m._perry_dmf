'''
Python 3 convert Date: 2/March/2020
'''

import copy, math
class Cyclic_Array():
    def __init__(self, N=1, InitVal=0, InitVec=None):
        if InitVec == None:
            self.V = [InitVal]*N
        else:
            self.V = InitVec
        self.idx = 0
        self.N = len(self.V)

    def step(self, new_val):
        old_val = self.V[self.idx]
        self.V[self.idx] = new_val
        self.idx += 1
        if self.idx >= self.N:
            self.idx = 0
        return old_val

def E(X, Y=None):
    S = 0
    if Y==None:
        sum(X)
    else:
        for i, Xi in enumerate(X):
            S += Xi*Y[i]
    return S/len(X)
 
class Stat_Recursive_X_Array():
    def __init__(self, X=[0]):
        self.CycAry = Cyclic_Array( InitVec=X)

        #Summing the entire array for the first and only time
        self.Sx = 0
        self.Sx2 = 0
        for i,Xi in enumerate(X):
            self.Sx += Xi
            self.Sx2 += Xi*Xi

        #Calculating the finale result
        N = self.CycAry.N
        self.E_x = self.Sx/N
        self.E_x2 = self.Sx2/N

    def step(self, Xi):
        #Cycling the array with a new value
        Xo = self.CycAry.step(Xi)
        N = self.CycAry.N

        #Updating the new sum of the array
        self.Sx -= Xo
        self.Sx += Xi
        self.E_x = self.Sx/N
        
        #Updating the new sum of the array
        self.Sx2 -= Xo*Xo
        self.Sx2 += Xi*Xi
        self.E_x2 = self.Sx2/N

        N2 = N*N
        self.Var = (1.0/(N*N-N)) * (N*self.Sx2 -self.Sx**2)
        return Xo

    def Ex(self):
        return self.E_x

    def Ex2(self):
        return self.E_x2

    def Var(self):
        return self.Var
