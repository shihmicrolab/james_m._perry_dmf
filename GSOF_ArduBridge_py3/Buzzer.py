"""
Class to build a simple sound playback.
The play function should be called with optional repeat value.
The default repeat value is 1.

By: Guy Soffer
Date: 20/Feb/2018
Python 3 convert Date: 2/March/2020
"""

import winsound

class Buzzer():
    def __init__(self, fileName, repeat=1):
        self.fileName = fileName
        self.repeat = repeat

    def play(self, repeat=False):
        if repeat == False:
            repeat = self.repeat

        while repeat > 0:
            repeat -= 1
            winsound.PlaySound(self.fileName, winsound.SND_FILENAME)
            
