'''
Python 3 convert Date: 2/March/2020
'''
class ArduBridge_Shield():
    def __init__(self, ardu):
        self.ardu = ardu

    def pwmA_init(self):
        self.pwm_init(dirPin=3, pwmPin=9)
        
    def pwmB_init(self):
        self.pwm_init(dirPin=4, pwmPin=5)

    def pwm_init(self, dirPin, pwmPin):
        for pin in [dirPin, pwmPin]:
            self.ardu.gpio.pinMode(pin,0)
            self.ardu.gpio.digitalWrite(pin,0)
    
    def pwmA(self, p):
            self.pwm(p,3,9)

    def pwmB(self, p):
            self.pwm(p,4,5)

    def pwm(self, p, dirPin, pwmPin):
        p = float(p)

        if p >= 0.0:
            if p > 100.0:
                p = 100.0
            val = 255*p/100.0
            di = 0
        else:
            if p < -100.0:
                p = -100.0
            val = 255*(1 +p/100.0)
            di = 1
        self.ardu.gpio.digitalWrite(dirPin, di)
        self.ardu.an.analogWrite(pwmPin, val)

    def ssrA(self, v):
        pin = 12
        if v < 0:
            v = 0
        elif v > 1:
            v = 1
        self.ardu.gpio.pinMode(pin,0)
        self.ardu.gpio.digitalWrite(pin,v)

    def ssrB(self, v):
        pin = 13
        if v < 0:
            v = 0
        elif v > 1:
            v = 1
        self.ardu.gpio.pinMode(pin,0)
        self.ardu.gpio.digitalWrite(pin,v)

    def pwmA_cur(self):
        return self.ardu.an.analogRead(2)

    def pwmB_cur(self):
        return self.ardu.an.analogRead(3)

