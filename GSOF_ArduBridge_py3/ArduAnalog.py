#!/usr/bin/env python
import GSOF_ArduBridge.BridgeSerial
import GSOF_ArduBridge.CON_prn

"""
Class to access the Arduino-Bridge Analog inputs and outputs.
This class is using the BridgeSerial class object to communicate over serial
with the Arduino-Bridge firmware.
The packet has a binary byte based structure
byte0 - 'P' for PWM-Out, 'A' for Analog-In
byte1 - pin bumber (binary-value)
byte2 - pwm-value (binary-value) only for analog-out command

By: Guy Soffer
Date: 20/Feb/2018
Python 3 convert Date: 2/March/2020
"""
class ArduBridgeAn():
    def __init__(self, bridge=False, v=False):
        self.v = v
        self.comm = bridge

    def analogWrite(self, pin, val):
        val = int(val)
        if (val > 0xff):
            val = 0xff
        if (pin < 0x1b):
            vDat = [ord('P'), pin, val]
            self.comm.send(vDat)
        reply = self.comm.receive(1)
        if self.v:
            RES = 'OK'
            if reply[0] == -1:
                RES = 'ERR'
            CON_prn.printf('PWM%d: %d - %s', par=(pin, val, RES), v=True)
        return reply[0]

    def analogRead(self, pin):
        if (pin < 0x1b):
            vDat = [ord('A'), pin]
            self.comm.send(vDat)
        reply = self.comm.receive(2)
        if reply[0] != -1:
            val = (reply[1][0] +(reply[1][1])*256)
            CON_prn.printf('AN%d: %d', par=(pin, val), v=self.v)
            return val
        CON_prn.printf('AN%d: Error', par=(pin), v=self.v)
        return -1
