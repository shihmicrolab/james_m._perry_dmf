"""
Script to build an ArduBridge environment
To customize the environment to your needs. You will need to change
he parameters in the "PARAMETER BLOCK" in the __main__ section

By: Guy Soffer
Date: 11/May/2018
Python 3 convert Date: 2/March/2020
"""

import socket, sys, time, threading

class udpControl():
    def __init__(self, nameID='', RxPort=7010, callFunc=False):
        self.nameID = str(nameID)
        self.callFunc = callFunc
        #self.ip = str(ipRX)
        self.RxPort = int(RxPort)
        #Datagram (UDP) socket
        self.active = False
        self.running = False
        ok = True
        try:
            self.udpRx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error as msg :
            print('Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
            ok = False
         
        if ok:
            # Bind socket to local host and port
            try:
                self.udpRx.setblocking(1)
                self.udpRx.bind(('', self.RxPort))
            except socket.error as msg:
                print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                ok = False

            print('%s: Ready on port %d\n'%(nameID, self.RxPort))
            self.active = True
            self.Thread = threading.Thread(target=self.run)
            self.Thread.start()

    def update(self, s):
        s = str(s)
        if self.callFunc != False:
            self.callFunc(s)
        else:
            print('%s: %s'%(self.nameID, s))

    def run(self):
        #print 'UDP-running...'
        self.running = True
        while self.active:
            #print 'UDP-running...'
            self.udpRx.settimeout(1)
            try:
                payload, fromIP = self.udpRx.recvfrom(512)
                payload = bytearray(payload)              #Add the binary payload
                self.update(payload)
            except socket.timeout:
                payload=[]
        self.running = False
        print('UDP-stoped...')
