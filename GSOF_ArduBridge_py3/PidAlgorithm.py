"""
Class to build an Enhanced-PID-Controller function.
The controller includes the following fetures:
- Rise-Time and Settle-Time measurements on each command.
- Enhanced antiwindup algorithm for the integrator.
- Configurable integrator limits.
- Configurable output limits.

The NextStep function should be called on periodic bases with new
control, feedback and Ts values.
It returns the new output value.
The class stores its state.

By: Guy Soffer
Date: 06/April/2018
Python 3 convert Date: 2/March/2020
"""

class PidAlgorithm():
    def __init__(self, P, I, D):
        self.Kp = P
        self.Ki = I
        self.Kd = D
        self.ctrl = 0
        self.feedback = [0]*3
        self.error = [0]*3
        self.delta = 0
        self.sum = 0
        self.diff = 0
        self.output = 0
        self.outMax = 255
        self.outMin = -255
        self.sumMax = 255
        self.sumMin = -255
        self.cycle = 0

    def NextStep(self, ctrl, feedback, dt):
        if dt < 0.0001:
            print('PID divid by zero! dt is too low')
            return
        self.cycle += 1
        self.ctrl = ctrl
        
        self.feedback.pop(0)
        self.feedback.append(feedback)

        self.error.pop(0)
        self.error.append(ctrl -feedback)

        self.delta = self.error[0] * self.Kp / dt

        if (self.Ki == 0):
            self.sum = 0
            dSum = 0
        else:
            dSum = self.error[0]*dt * self.Ki
            self.sum += dSum
            if self.sum > self.sumMax:
                self.sum = self.sumMax
            elif self.sum < self.sumMin:
                self.sum = self.sumMin


#        #Diff(f(x) = (f(x + h) - f(x - h)) / 2h
#        self.diff = self.D * (self.error[0] - self.error[2]) / 2*dt

        #Diff(f(x) = (f(x) - f(x - h)) / h
        self.diff = ((self.error[0] - self.error[1])/dt) * self.Kd
        
        self.output = self.delta +self.sum +self.diff

        #Clamp the output results to the limits
        if self.output > self.outMax:
           self.output = self.outMax #Clamp the output to maximum
        elif self.output < self.outMin:
           self.output = self.outMin #Clamp the output to minimum

        #Enhanced-Anti-Windup methode (By: Guy Soffer)
        if ((self.output == self.outMax) or (self.output == self.outMin)):
           if ( (self.sum<0) and (self.output<0) and (dSum<0)
                or
                (self.sum>0) and (self.output>0) and (dSum>0)
                ):
               self.sum -= dSum #Cancel the integration part

        return self.output

    def getStatus(self):
        s = 'Cyc: %d, '%(self.cycle) 
        s +='Ct: %6.1f, Fb: %6.1f, Delta: %6.2f, Sum: %6.2f, Diff: %6.2f, Out: %6.2f'%(self.ctrl,
                                                                                       self.feedback[0],
                                                                                       self.delta,
                                                                                       self.sum,
                                                                                       self.diff,
                                                                                       self.output)
        return s
        

