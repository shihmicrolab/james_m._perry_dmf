#!/usr/bin/env python
import serial, time, threading

"""
Class of basic serial communication methods.
The class object is build using the supplied serial device name and boud-rate as an arguments.
The class methodes send and receive are thread protected using internal semaphores.
Special data bytes as RST(0x1b) and ESC(0x5c) are send using an escape code sequence.
The escape code sequence is generated as follow:
Instead of sending the value 0x1b, two bytes will be sent 0x5c followed by 0xb1
Instead of sending the value 0x5c, two bytes will be sent 0x5c followed by 0xc5
All rest of the values are sent without any manipulations.

By: Guy Soffer
Date: 20/Feb/2018
Python 3 convert Date: 2/March/2020
"""
class ArduBridgeComm():
    """ Open, Close, Send, Receive methods """
    RST  = 0x1b
    ESC  = 0x5c
    CR   = 0x0d
    LF   = 0x0a
    ERR  = -1

    def __init__(self, COM='COM1', baud=9600, v=False, PortStatusReport=False):
        self.RxTimeOut = 0.005
        self.RxTry = 5
        self.verbose = v
        self.LINK = False
        self.semaTX = threading.Semaphore(1)
        self.semaRX = threading.Semaphore(1)
        self.ser = serial.Serial(None,
                                 baud,
                                 timeout=self.RxTimeOut,
                                 writeTimeout=900
                                 )
        self.ser.port = COM
#        self.LinkReport = LinkReport
#        self.PortStatusReport = PortStatusReport
#        CON_prn.printf("%s> Verbose-ON", (self.nameID), v=self.verbose)
#        CON_prn.printf("%s> LINK is configured to %s at %dbps", (self.nameID, COM, baud), v=True)

    def sendReset(self):
        self.semaTX.release()
        self.uart_wr(chr(self.RST))
        
    def send(self, vDat):
        self.semaTX.acquire()
        if ( self.LINK == True):
            self.ser.flushInput() #self.ser.read(32) #self.uart_flush() #Make sure RX is empty
            vStr = ''
            for c in vDat:
                if ( (c == self.ESC) or (c == self.RST) ):
                    swap_c = ((c&0xf)<<4) +((c>>4)&0xf)
                    vStr += ( chr(self.ESC) +chr(swap_c) )
                else:
                    vStr += ( chr(c) )
            self.uart_wr( vStr )
        self.semaTX.release()

    def getByte(self):
        c = []
        trail = self.RxTry
        while (len(c)==0) and (trail > 0):
            c = self.ser.read(1)
            trail -= 1
        if len(c) == 0:
            print('Error - %s RX timeout'%(self.ser.port))
        return c
        
    def receive(self, N):
        self.semaRX.acquire()
        vDat = [-1]*N
        ERR = [self.ERR]
        if (self.LINK == True):
            ERR = [0, vDat]
            i = 0
            while i<N:
                c = self.getByte()
                
                if len(c) == 0:
                    self.semaRX.release()
                    return ERR
                c = ord(c)
                
                if c == self.ESC:
                    c = self.getByte()
                    if len(c) == 0:
                        self.semaRX.release()
                        return ERR
                    c = ord(c)
                    c = ((c&0xf)<<4) +((c>>4)&0xf) #Swapping the value after ESC
                    
                if c == self.RST:
                    print('Error - Received RST from Arduino-Bridge\n')
                    self.semaRX.release()
                    return ERR
                vDat[i] = c
                i += 1
            self.semaRX.release()
            return [1, vDat]
        print('Error - %s is closed'%(self.ser.port))
        self.semaRX.release()
        return ERR

    def ReportLinkStatus(self, val):
        self.LINK = val
        if (self.LinkReport):
            self.LinkReport(val)
        
    def OpenClosePort(self, val):
        if (val):
            try:
                self.ser.open()
                self.LINK = True
                print('ArduBridge COM is open')
                
            except serial.SerialTimeoutException:
                self.LINK = False
                print(' Error - Cannot oprn %s\n'%(self.ser.port))
                pass

            except serial.SerialException:
                print('Cannot execute command\n')
        else:
            self.ser.close()
            self.LINK = False
            print('ArduBridge COM is closed')

    def uart_flush(self):
        if (self.LINK):
            #self.ser.flush()
            v=[1]
            while len(v) > 0:
                v = self.ser.read(32)

    def uart_wr(self, dat):
        #print dat
        if (self.LINK):
            try:
                dat = dat.encode() ####convert to byte data first????
                self.ser.write(dat)
                
            except serial.SerialTimeoutException:
                print('Error - %s TX timeout'%(self.ser.port))
                #self.ReportLinkStatus(False)
                pass
           
