#!/usr/bin/env python
#def printf(str, par=(), data='', v=True):
#  if v:
#    print str % par, data
'''
Python 3 convert Date: 2/March/2020
'''

def printf(text, par=(), data='', v=True, file=False):
    PrintLine = text % par
    PrintLine += str(data) +' '
#    if (type(data) == 'list') or (type(data) == 'tuple'):
#        for val in data:
#            PrintLine += str(val) +' '
#    else
#        PrintLine += str(data) +' '
    
    if PrintLine[-1]=='\n':
        PrintLine = PrintLine[:-1]
    if v:
        print(PrintLine)
    if (file):
        if (file.closed == False):
            file.write(PrintLine+'\n')
