"""
General UDP transmitter.
1. Send a any type of list to the specific ip/port.

By: Guy Soffer
Date: 20/Feb/2018
Python 3 convert Date: 2/March/2020
"""

import socket

class udpSend():
    def __init__(self, nameID='', DesIP='127.0.0.1', DesPort=6000):
        self.nameID = str(nameID)
        self.DesIP = str(DesIP)
        self.DesPort = int(DesPort)
        #Datagram (UDP) socket
        try:
            self.udpTx = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            print('%s: Ready to send to %s:%d\n'%(nameID, self.DesIP, self.DesPort))
        except socket.error as msg :
            print('Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                 
    def Send(self, s):
        buf = str(bytearray(str(s)))
        self.udpTx.sendto( buf,(self.DesIP, self.DesPort) )
