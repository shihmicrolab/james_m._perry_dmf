#!/usr/bin/env python
from GSOF_ArduBridge import BridgeSerial
from GSOF_ArduBridge import CON_prn

"""
Class to access the Arduino-Bridge digital inputs and outputs.
This class is using the BridgeSerial class object to communicate over serial
with the Arduino-Bridge firmware.
The packet has a binary byte based structure
byte0 - 'D' to set pin direction, 'I' to read pin state, 'O' to set pin state
byte1 - pin bumber (binary-value)
byte2 - pin-value (binary-value) only for digital-out command

By: Guy Soffer
Date: 06/Aug/2018
Python 3 convert Date: 2/March/2020
"""
import time

class ArduBridgeGPIO():
    def __init__(self, bridge=False, v=False):
        self.v = v
        self.comm = bridge
        self.RES = {1:'OK', 0:'ERR' , -1:'ERR'}
        self.DIR = {1:'IN', 0:'OUT'}

    def pinMode(self, pin, mode):
        if (mode != 0):
            mode = 1
        if (pin < 0x1b):
            vDat = [ord('D'), pin, mode]
            print(str(vDat))
            self.comm.send(vDat)
        reply = self.comm.receive(1)
        print(reply)
        CON_prn.printf('DIR%d: %s - %s', par=(pin, self.DIR[mode], self.RES[reply[0]]), v=self.v)
        return reply[0]

    def digitalWrite(self, pin, val):
        val = int(val)
        if (val != 0):
            val = 1
        if (pin < 0x1b):
            vDat = [ord('O'), pin, val]
            self.comm.send(vDat)
        reply = self.comm.receive(1)
        CON_prn.printf('DOUT%d: %d - %s', par=(pin, val, self.RES[reply[0]]), v=self.v)
        return reply[0]

    def servoWrite(self, val):
        val = int(val)
        vDat = [ord('S'), val]
        self.comm.send(vDat)
        reply = self.comm.receive(1)
        CON_prn.printf('SERVO: %d - %s', par=(val, self.RES[reply[0]]), v=self.v)
        return reply[0]

    def pinPulse(self, pin, onTime):
        """
        Pulse the the specific pin# on the arduino GPO
        """
        self.digitalWrite(pin, 1)
        time.sleep(onTime)
        self.digitalWrite(pin, 0)
        return 1

    def digitalRead(self, pin):
        if (pin < 0x1b):
            vDat = [ord('I'), pin]
            self.comm.send(vDat)
        reply = self.comm.receive(1)
        if reply[0]:
            val = reply[1][0]
            CON_prn.printf('DIN%d: %d', par=(pin, val), v=self.v)
            return val
        CON_prn.printf('DIN%d: Error', par=(pin), v=self.v)
        return -1


