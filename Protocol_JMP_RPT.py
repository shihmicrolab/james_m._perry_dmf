"""
Pyhton 3 convert Date: 2/Mark/2020
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
READ.ME~~~~~~~James, Jan 2018~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~Instructions for this protocol:
1.Open ArduBridge.py in Python IDE, enter following where required:

COM port (arudino connection), MagPin (digital pin number to which solednoid is connected), Kenza_cfpsspe_jan18

2.Save ArduBridge.

3.Run ArduBridge (F5)

4.To run your protocol, enter  following in Shell:

To enable electrodes:
setup.enOut()

To run the script, type:
prot.start()

To pause the protocol:
prot.pause()
prot.cont()

To stop the ptotocol:
prot.pause()
prot.stop()
prot.reset()

To close the threads, everything, type:
setup.close()
prot.stop()

~~TROUBLESHOOT~~

When you turn power supply on/off, you have to reconfigure the electrode switch stack.
Run >>> ExtGpio.init()

To test individual electrodes run >>> ExtGpio.Write('pinnr','1/0') and probe with osciplloscope.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""

from GSOF_ArduBridge import ElectrodeGpioStack
from GSOF_ArduBridge import threadElectrodeSeq
from GSOF_ArduBridge import threadBasic as bt
import threading
import time
import seqList_JMP_RPT9 as SL
    
class Setup():

    ''' The code listed below sets up the infrastructure of a
     chip repeatable design elements. Each element in the list
     SL.seqList.t set of electrodes making up a sequence unit,
     such as eqList.t[1] to SL.seqList.t[9] are tracks of
     electrodes between voirs, 11-19 are sequences for
     dispencing from resevoirs ted on the top of the device.
     Each category occupies 10 ents to make it easier to sort
     the list. For example 0-9: king, 10-19: Dispencing from the
     top, 20-29: Dispencing from bottom, etc. '''
            
    def __init__(self, ExtGpio, gpio, chipViewer, UDP_send = False):
        self.ExtGpio = ExtGpio
        self.gpio = gpio
        self.chipViewer = chipViewer
        self.UDP_send = UDP_send
        self.seq={}
      
  # \/ Sequence block start \/
        seqName = 'seq1'
        self.seq[seqName] = threadElectrodeSeq.MoveElecSeqThread(
                                                    gpio=ExtGpio,
                                                    nameID=seqName, #The name of the protocol
                                                    Period=0.5, #Total time [Sec]
                                                    onTime=0.4, #On time for each electrod [Sec]
                                                    elecList=[] #The list itself
                                                    )

        self.seq[seqName].addViewer('UDP', chipViewer)
        # /\ Sequence block send  /\

        

##        self.t=[]
##        #1 Tracking: these are the sequences for each section of 'tracking electrodes'
##        self.t.append([0]) #Empty space so self.t[0] is empty        
##        self.t.append( 	[67, 14, 15, 69, 16, 70]    )
##        self.t.append( 	[71, 18, 19, 73, 20, 74]    )
##        self.t.append( 	[75, 22, 23, 78, 24, 79]    )
##        self.t.append( 	[0] )
##        self.t.append( 	[0] )
##        self.t.append( 	[0] )
##        self.t.append( 	[0] )
##        self.t.append( 	[0] )
##        self.t.append([0]) #Empty space
##        self.t.append([0]) #Empty space
##
##        #11 Dispensing from the top
##        self.t.append( 	[12, 13, 67,12]	)
##        self.t.append( 	[16, 17, 71,16]	)
##        self.t.append( 	[20, 21, 75, 20] )
##        self.t.append( 	[0]	)
##        self.t.append( 	[0]	)
##        self.t.append( 	[0]	)
##        self.t.append( 	[0]	)
##        self.t.append( 	[0]	)
##        self.t.append( 	[0]	)
##        self.t.append([0]) #Empty space
##        
##        #21 Dispensing from the bottom
##        self.t.append( 	[62, 85, 6, 62]	)
##        self.t.append( 	[64, 87, 8, 64]	)
##        self.t.append( 	[66, 89, 10, 66]	)
##        self.t.append( 	[ 91, 12, 68]	)
##        self.t.append( 	[93, 14, 70]	)
##        self.t.append( 	[95, 16, 72]	)
##        self.t.append( 	[74, 97, 18, 74]	)
##        self.t.append( 	[76, 99, 20, 76]	)
##        self.t.append( 	[78, 101, 22, 78]	)
##
##        self.t.append([0]) #Empty space
##        #31 Delivering from the top
##        self.t.append( 	[6, 29, 5, 6]	)
##        self.t.append( 	[8, 31, 7 , 8]	)
##        self.t.append( 	[10, 33, 9, 10]	)
##        self.t.append( 	[13, 35, 11, 13]	)
##        self.t.append( 	[14, 37, 13 , 14]	)
##        self.t.append( 	[16, 39, 15, 16]	)
##        self.t.append( 	[18, 41, 17, 18]	)
##        self.t.append( 	[20, 43, 19, 19]	)
##        self.t.append( 	[22, 45, 21, 22]	)
##
##        self.t.append([0]) #Empty space
##        #41 Delivering from the bottom
##        self.t.append( 	[85, 62]	)
##        self.t.append( 	[8, 63,[ 8, 63, 87], 87, 64]	)
##        self.t.append( 	[10, 65,[ 10, 65, 89], 89, 66]	)
##        self.t.append( 	[12, 67,[ 12, 67, 91], 91, 68]	)
##        self.t.append( 	[14, 69,[ 14, 69, 93], 93, 70]	)
##        self.t.append( 	[16, 71, [16, 71, 95], 95, 72]	)
##        self.t.append( 	[18, 73, [18, 73, 97], 97, 74]	)
##        self.t.append( 	[20, 75,[20, 75, 99], 99, 76]	)
##        self.t.append( 	[22, 77, [22, 77, 101], 101, 78]	)
##
##        self.t.append([0]) #Empty space
##        #51 mix top
##        self.t.append( 	[5, 29, 61, 29, 5, 29]	)
##        self.t.append( 	[7, 31, 63, 31, 7, 31]	)
##        self.t.append( 	[9, 33, 65, 33, 9, 33]	)
##        self.t.append( 	[11, 35, 67, 35, 11, 35]	)
##        self.t.append( 	[13, 37, 69, 37, 13, 37]	)
##        self.t.append( 	[15, 39, 71, 39, 15, 39]	)
##        self.t.append( 	[17, 41, 73,73, 41, 17,17 ]	)
##        self.t.append( 	[19, 43, 75,75, 43, 19,19 ]	)
##        self.t.append( 	[21, 45, 77,77, 45, 21,21 ]	)
##
##
##        self.t.append([0]) #Empty space
##        #51 mix bottom
##        self.t.append( 	[62, 85, 6, 85, 62, 85]	)
##        self.t.append( 	[64, 87, 8, 87, 64, 87]	)
##        self.t.append( 	[66, 89, 10, 89, 66, 89]	)
##        self.t.append( 	[68, 91, 13, 91, 68, 91]	)
##        self.t.append( 	[70, 93, 14, 93, 70, 93]	)
##        self.t.append( 	[72, 95, 16, 95, 72, 95]	)
##        self.t.append( 	[74, 97, 18, 97, 74, 97]	)
##        self.t.append( 	[76, 99, 20, 99, 76, 99]	)
##        self.t.append( 	[78, 101, 22, 101, 78, 101]	)
        
        self.seqList=SL.seqList()
        self.genSeq = []
        self.sgenSeq = []
        
        self.genSeq.append(threadElectrodeSeq.MoveElecSeqThread(
                                                                gpio=ExtGpio,
                                                                nameID='genSeq1', #The name of the protocol
                                                                Period=1.0, #Total time [Sec]
                                                                onTime=0.7, #On time for each electrod [Sec]
                                                                elecList=[] #The list itself
                                                                )
                           )

        self.sgenSeq.append(threadElectrodeSeq.ArduElecSeqThread(
                                                                gpio=self.ExtGpio,
                                                                nameID='sgenSeq1', #The name of the protocol
                                                                Period=1.0, #Total time [Sec]
                                                                onTime=0.7, #On time for each electrod [Sec]
                                                                elecList=[] #The list itself
                                                                )
                            )
        self.enOut(False)

    def printSeq(self):
        for key in list(self.seq.keys()):
            print(key)


    """The following methods, SEQUENCE FINDERS, take the sequence lists and sorts them to build
    droplets paths. For example, disp(0,1) takes the orientation (0 =
    top) and block (1 = first block) of a chip resevoir and finds the
    appropriate sequence from the list (self.t[11]). def disp(self, ori,
    blk):"""

    def teleUpdate(self,tele):
        if self.UDP_send == False:
            print(tele)
        else:
            tele = str(tele)
            self.UDP_send(tele)
    #################SEQUENCE FINDERS##################
    
    def dispFind(self, ori,blk):
        """ Returns a list of electrodes sequence to 'dispense' a droplet
        from the dispensing area to the track """
        blk -= 1
        if (ori == 0) or (ori == 1):
            d = 11
            if ori == 1:
                d += 10
            d += blk
            return self.seqList.t[d] +[self.seqList.t[d][-1]]
        self.teleUpdate('Input error')
        return []

            
    def deliFind(self, ori, blk):
        """ Returns a list of electrodes to 'deliver' a droplet from the
        track to the incubator """
        blk -= 1
        if (ori == 0) or (ori == 1):
            d = 31
            if ori == 1:
                d += 10
            d += blk
            return self.seqList.t[d]+[(self.seqList.t[d])[-1]]
        self.teleUpdate('Input error')
        return []

    def trackFind(self, bgBlock, endBlock):
        """ Returns a sequence to move a droplet from a source block to
        destination block on the delivery track """
        seq = []
        if bgBlock == endBlock:
            return []
    
        for i in range(bgBlock,endBlock):
            seq += self.seqList.t[i]
        if (bgBlock < endBlock):
            return seq + [seq[-1]]
        elif (bgBlock > endBlock):
            return seq.reverse() + seq[0]
        
    def mixFind(self, ori, blk):
        """ Returns a list of electrodes sequence for mixing """
        blk -= 1
        if (ori == 0) or (ori == 1):
            d = 51
            if ori == 1:
                d += 10
            d += blk
            return self.seqList.t[d] +[self.seqList.t[d][-1]]
        self.teleUpdate('Input error')
        return []

    def mix2Find(self, ori, blk):
        """ Returns a list of electrodes sequence for mixing """
        blk -= 1
        if (ori == 0) or (ori == 1):
            d = 71
            if ori == 1:
                d += 10
            d += blk
            return self.seqList.t[d] +[self.seqList.t[d][-1]]
        self.teleUpdate('Input error')
        return []


    ######################################################

    """The following methods allow for electrode actuation"""    

    def enOut(self, val=True):
        self.outputEnale = val
        for key in list(self.seq.keys()):
            self.seq[key].enOut = self.outputEnale
        for seq in self.genSeq:
            seq.enOut = self.outputEnale

    def stop(self):
        for key in list(self.seq.keys()):
            self.seq[key].stop()
        for seq in self.genSeq:
            seq.stop()

    def addSeq(self, lst=[]):
        x=[]
        for s in lst:
            x += self.seqlist.t[s]
        x.append(x[-1])
        return x

    def startNewSeq(self, seqName):
        if seqName == 'T1':
            seq = [11,12]
        elif seqName== 'T2':
            seq = [1,2]
        elif seqName== 'T3':
            seq = [11,22]
        elif seqName== 'T4':
            seq = [3,1,2]
        else:
            self.teleUpdate('Wrong sequence name')
            return

        genSeq = 'dis2a'
        self.seq[genSeq].elecList = self.addSeq(seq)
        self.seq[genSeq].start(1)

    def startSeq(self, elecList, N=1, onTime=0.5, Period=1.0):
        freeSeq = False
        for seq in self.genSeq:
            if seq.enable == False:
                freeSeq = seq
        if freeSeq == False:
            seqName = 'genSeq%d'%(len(self.genSeq)+1)
            self.teleUpdate('Building new genSeq %s'%(seqName))

            self.genSeq.append(threadElectrodeSeq.MoveElecSeqThread(gpio=self.ExtGpio,
                                                                    nameID=seqName, #The name of the protocol
                                                                    Period=Period, #Total time [Sec]
                                                                    onTime=onTime, #On time for each electrod [Sec]
                                                                    elecList=elecList #The list itself
                                                                    )
                                   )
            freeSeq = self.genSeq[-1]
        else:
            self.teleUpdate('Using %s'%(freeSeq.name))
            freeSeq.Period=Period #Total time [Sec]
            freeSeq.onTime=onTime #On time for each electrode [Sec]
            freeSeq.elecList=elecList #The list itself
        freeSeq.enOut = self.outputEnale
        freeSeq.start(N)
        #return freeSeq.queue


        #############################
    def startSeqs(self, elecList, N=1, onTime=0.5, Period=0.5):
        freeSeq = False
        for seq in self.sgenSeq:
            if seq.enable == False:
                freeSeq = seq
        if freeSeq == False:
            seqName = 'sgenSeq%d'%(len(self.genSeq)+1)
            self.teleUpdate('Building new sgenSeq %s'%(seqName))
          
            self.sgenSeq.append(threadElectrodeSeq.ArduElecSeqThread(gpio=self.ExtGpio,
                                                                    nameID=seqName, #The name of the protocol
                                                                    Period=Period, #Total time [Sec]
                                                                    onTime=onTime, #On time for each electrod [Sec]
                                                                    elecList=elecList #The list itself
                                                                    )
                               )
            freeSeq = self.genSeq[-1]
        else:
            self.teleUpdate('Using %s'%(freeSeq.name))
            freeSeq.Period=Period #Total time [Sec]
            freeSeq.onTime=onTime #On time for each electrode [Sec]
            freeSeq.elecList=elecList #The list itself
        freeSeq.enOut = self.outputEnale
        freeSeq.start(N)
        #return freeSeq.queue



    """The following methods, SEQUENCE EXECUTERS, take droplet path
    sequences built by the SEQUENCE FINDERS and supplies them to the
    startseq() method which builds threadElectrodeSeq objects and
    starts them, which turns on and off electrodes."""
    
    #################SEQUENCE EXECUTERS##################    

    def dispExec(self, blk, ori,p=0.5,trigger=False,*args):
        
        elecList = self.dispFind(ori, blk)
        self.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time

    def trackExec(self, bgBlock, endBlock,p=0.5):
        elecList = self.trackFind(bgBlock, endBlock)
        if bgBlock != endBlock:
            self.startSeq( elecList= elecList,
                                N=1,
                                onTime=-1,
                                Period=p
                                )
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time
    
    def deliExec(self, ori, blk,p=0.5):
        
        elecList = self.deliFind(ori, blk)
        self.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time

    def mixExec(self, ori, blk,p=0.5,n=1):
        
        elecList = self.mixFind(ori, blk)
        self.startSeq( elecList= elecList,
                            N=n,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList*n)*(p+1.1)
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time

    def mix2Exec(self, ori, blk,p=0.5,n=1):
        
        elecList = self.mix2Find(ori, blk)
        self.startSeq( elecList= elecList,
                            N=n,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList*n)*(p*1.1)
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time
    

##    def tracking(self, bgBlock, endBlock,p=0.5):
##        """ starts a sequence to move a droplet on the track (left to
##        right) """
##        seq = self.seq['dis2a']
##        seq.elecList = self.autoTrack(bgBlock, endBlock)
##        print seq.elecList
##        seq.start(1)
##        return p



        
    def distro(self,b,o,d,of,p=0.5):
        #elecList = self.dispFind(o,b)[0:-1]+self.trackFind(b,d)[0:-1]+self.deliFind(of,d)[1:-1]
        elecList = self.dispFind(o,b)[0:-1]+self.trackFind(b,d)[0:-1]+self.deliFind(of,d)
        self.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time

    def dx(self,b,o,d,of,p=0.5):
        elecList = self.dispFind(o,b)#[0:-1]+self.trackFind(b,d)[0:-1]+self.deliFind(of,d)
        self.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=p
                            )
        self.genSeq[-1].event.wait()
        print('killler')
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time
##    def bring(self,b,o,d,of,p1=0.5,p2=0.5,p3=0.5):
        

    def flip(self,b,o,p=0.5):
        if o == 0:
            of = 1
        if o == 1:
            of = 0
        elecList = self.dispFind(o,b)[0:-1]+self.deliFind(of,b)
        #seq.elecList = [i-1 for i in seq.elecList]  #inverted
        
        self.startSeq( elecList= elecList,
                            N=1,
                            onTime=-1,
                            Period=p
                            )
        time = len(elecList)*p
        self.teleUpdate('total time: %3.2f'%(time))
        self.teleUpdate(elecList)
        return time
    
    def seq(elist=[],p=1,t=.9,n=1):
        elecList = elist
        self.startSeq( elecList= elecList,
                            N=n,
                            onTime=t,
                            Period=p,
                            move=False
                            )

    def test(self):
        while True:
            time.sleep(5)
            self.teleUpdate(time.time())
    
        


##t1=[6,65,7,31,89,66]
##t2=[62,63,5,29,87,64]
##t3=[8, 67, 9, 33, 91, 68]
##t4=[10, 69, 11, 35, 93, 70]
##t5=[12, 71, 13, 37, 95, 72]
##t6=[14, 73, 15, 39, 97, 74]
##t7=[16, 75, 17, 41, 99, 76]
##t8=[18, 77, 19, 43, 101, 78]

