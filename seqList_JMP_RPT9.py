'''
Python 3 convert Date: 2/March/2020
'''
class seqList():
    def __init__(self):
        
        self.t=[]
        #1 Tracking
        self.t.append([0]) #Empty space        
        self.t.append( 	[6, 30, 62, 7, 86, 63])
        self.t.append( 	[8, 32, 64, 9, 88, 65])
        self.t.append( 	[10, 34, 66, 11, 90, 67])
        self.t.append( 	[12, 36, 68, 13, 92, 69])
        self.t.append( 	[14, 38, 70, 15, 94, 71])
        self.t.append( 	[16, 40, 72, 17, 96, 73,])
        self.t.append( 	[18, 42, 74, 19, 98, 75])
        self.t.append( 	[20, 44, 76, 21, 100, 77])
        self.t.append([0]) #Empty space
        self.t.append([0])    #Empty space
        
        #11 Dispensing from the top
        self.t.append( 	[29, 61, 5] )
        self.t.append( 	[31, 63, 7] )
        self.t.append( 	[33, 65, 9] )
        self.t.append( 	[35, 67, 11])
        self.t.append( 	[37, 69, 13])
        self.t.append( 	[39, 71, 15])
        self.t.append( 	[41, 73, 17])
        self.t.append( 	[43, 75, 19])
        self.t.append( 	[45, 77, 21])

        self.t.append([0]) #Empty space
        #21 Dispensing from the bottom
        self.t.append( 	[85, 6, 62]	)
        self.t.append( 	[87, 8, 64]	)
        self.t.append( 	[89, 10, 66]	)
        self.t.append( 	[91, 12, 68]	)
        self.t.append( 	[93, 14, 70]	)
        self.t.append( 	[95, 16, 72]	)
        self.t.append( 	[97, 18, 74]	)
        self.t.append( 	[99, 20, 76]	)
        self.t.append( 	[101, 22, 78]	)

        self.t.append([0]) #Empty space
        #31 Delivering from the top
        self.t.append( 	[61, 6, 29, 5]	)
        self.t.append( 	[63, 8, 31, 7]	)
        self.t.append( 	[65, 10, 33, 9]	)
        self.t.append( 	[67, 12, 35, 11])
        self.t.append( 	[69, 14, 37, 13])
        self.t.append( 	[71, 16, 39, 15])
        self.t.append(  [73, 18, 41, 17])
        self.t.append( 	[75, 20, 43, 19])
        self.t.append( 	[77, 22, 45, 21])

        self.t.append([0]) #Empty space
        #41 Delivering from the bottom
        self.t.append( 	[61,85,62]	)
        self.t.append( 	[63, 87, 64]	)
        self.t.append( 	[65, 89, 66]	)
        self.t.append( 	[67, 91, 68]	)
        self.t.append( 	[69, 93, 70]	)
        self.t.append( 	[71, 95, 72]	)
        self.t.append( 	[73, 97, 74]	)
        self.t.append( 	[75, 99, 76]	)
        self.t.append( 	[77, 101, 78]	)

        self.t.append([0]) #Empty space
        #51 mix top
        self.t.append( 	[5, 29, 61, 29, 5, 29]	)
        self.t.append( 	[7, 31, 63, 31, 7, 31]	)
        self.t.append( 	[9, 33, 65, 33, 9, 33]	)
        self.t.append( 	[35, 67, 12, 90, 11]	)
        self.t.append( 	[92,69,14,37,93]	)
        self.t.append( 	[15, 39, 71, 39, 15, 39])
        self.t.append( 	[41,73,18,96,17])
        self.t.append( 	[43,75,20,98,19])
        self.t.append( 	[45,77,22,100,21])


        self.t.append([0]) #Empty space
        #61 mix bottom
        self.t.append( 	[62, 85, 6, 85, 62, 85]	)
        self.t.append( 	[64, 87, 8, 87, 64, 87]	)
        self.t.append( 	[66, 89, 10, 89, 66, 89])
        self.t.append( 	[68, 91, 13, 91, 68, 91])
        self.t.append( 	[70, 93, 14, 93, 70, 93])
        self.t.append( 	[72, 95, 16, 95, 72, 95])
        self.t.append( 	[97,18,73,96,74])
        self.t.append( 	[99,20,75,98,76])
        self.t.append( 	[101,22,77,100,78])

        self.t.append([0]) #Empty space
        #71 mix_simp top
        self.t.append( 	[5, 29, 61, 29, 5, 29]	)
        self.t.append( 	[7, 31, 63, 31, 7, 31]	)
        self.t.append( 	[9, 33, 65, 33, 9, 33]	)
        self.t.append( 	[35, 67, 12, 90, 11]	)
        self.t.append( 	[92,69,14,37,93]	)
        self.t.append( 	[15, 39, 71, 39, 15, 39])
        self.t.append( 	[41, 17, 41, 41, 17, 41, 41, 17, 41, 41, 73, 18, 96, 73, 18, 41, 17, 0])
        self.t.append( 	[43, 19, 43, 43, 19, 43, 43, 19, 43, 43, 75, 20, 98, 75, 20, 43, 19, 0])
        self.t.append( 	[45, 21, 45, 45, 21, 45, 45, 21, 45, 45, 77, 22, 100, 77, 22, 45, 21, 0])


        self.t.append([0]) #Empty space
        #81 mix_simp
        self.t.append( 	[62, 85, 6, 85, 62, 85]	)
        self.t.append( 	[64, 87, 8, 87, 64, 87]	)
        self.t.append( 	[66, 89, 10, 89, 66, 89])
        self.t.append( 	[68, 91, 13, 91, 68, 91])
        self.t.append( 	[70, 93, 14, 93, 70, 93])
        self.t.append( 	[72, 95, 16, 95, 72, 95])
        self.t.append( 	[97, 74, 97, 97, 74, 97, 97, 74, 97, 97, 18, 73, 42, 18, 73, 97, 74, 0])
        self.t.append( 	[99, 76, 99, 99, 76, 99, 99, 76, 99, 99, 20, 75, 44, 20, 75, 99, 76, 0])
        self.t.append( 	[101, 78, 101, 101, 78, 101, 101, 78, 101, 101, 22, 77, 46, 22, 77, 101, 78, 0])



##    for i in range(9):
##	for n in range(len(l)):
##		l[n]+=2
##	l
