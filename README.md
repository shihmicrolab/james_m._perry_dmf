# James M. Perry et al.'s DMF system 

Authors: James M. Perry & Guy Soffer

This repository contains the python-based system used in
perry et al. 2021. This system is built to use the GSOF Ardubridge
python software, arduino firmware and shield harware as well as 
DMF hardware which can be found here:

https://bitbucket.org/shihmicrolab/gsof_ardubridge_py3 (currently under contruction)



This system includes JMP_MOD_lib (module library)
including:

## ElecVol_8
A class to control droplet dispensing to electrodes using impedance-based feedback

## ReplenishManager
A class to manage multiple instances of ElecVol_8 

## setFuncClass & impScan
classes to control a fucntion generator used in DMF and impedance-based volume detection

## stepperMotor
A class to operate a low rpm stepper motor used for a syringe pump
 
Additional information for a basic (compatible) temperature control system can be found in 
temp_control_system_basic with an aduino .ino file for operation. 


#### To start

1. Clone repo
2. Clone and setup gsof_ardubridge_py3
3. Compile firmware onto arduino
4. Setup seqList file as per your chip design 
5. Run ArduBridge_JMP_RPT.py or ArduBridge_basic.py using idle.exe
6. Refer to Ardubridge files for idle shell funcitons

