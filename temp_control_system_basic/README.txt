This is a basic implimentation of the temperature control system used in:
Perry et al. 2021
DOI	https://doi.org/10.1039/D1LC00415H

For the extensive python based GSOF ArduBridge-compatible system, please refer to:

https://bitbucket.org/shihmicrolab/james_m._perry_dmf

###Setup notes###

Please refer to the included diagram.png for setup. The system should work with any 
motor driver and peltier with wattage taken into consideration. A two-channel DC power 
supply is essential For this system. A Tetech TE-63-1.4-1.15 peltier was used with a 
10k bead thermistor NXFT15XH103FA2B060:

https://www.digikey.ca/en/products/detail/murata-electronics/NXFT15XH103FA2B060/7595835

Motor Driver:

https://www.amazon.com/H-Bridge-Control-IRF3205-Response-Arduino/dp/B08JM4Z1HP/ref=sr_1_11_mod_primary_new?crid=BCGMGRQBUMLQ&keywords=motor+driver+dual&qid=1660687356&sbo=RZvfv%2F%2FHxDF%2BO5021pAnSA%3D%3D&sprefix=motor+driver+duel+%2Caps%2C65&sr=8-11

Refer to datasheet for correct Steinhart-hart beta coeffecient (p. 6 of 
NXFT15XH103FA2B060 data sheet) and make adjustments to code (line 25). 

###use###

1. Setup harware
2. Make sure you have Arduino IDE downloaded
2. Download PID library (see included arduino code for details)
3. Upload sketch "tempControl.ino" to arduino 
4. Input desired temperature via Arduino IDE serial monitor

Trouble shooting:

-Connect the logic-side ground of motor driver and arduino ground to power supply
-to view raw analog signal of thermistor (analog pin val), uncomment line 106


