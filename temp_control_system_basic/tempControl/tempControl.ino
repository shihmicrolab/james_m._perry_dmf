//Arduino-based temperature control system based on https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/
//Use: 
//1. setup as per included diagram 
//2. add PID package: Tools>Manage Libraries...>*type 'PID'. Select PID (by Brett Beauregard) See https://playground.arduino.cc/Code/PIDLibrary/
//3. upload script to arduino
//4. open serial monitor and input temperature setpoint eg. 30 -> results in 30 deg C



#include <PID_v1.h>
unsigned long time;
const int PWM = 3;
const int DIR = 2;

/////////////////////////////////////////////thermistor//////////////////////////////////////////////
// which analog pin to connect
#define THERMISTORPIN A0         
// resistance at 25 degrees C
#define THERMISTORNOMINAL 10000      
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25   
// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 3
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 4040
// the value of the 'series' resistor
#define SERIESRESISTOR 10000 // different resistances will result in better resolution in different temperature ranges    
 
uint16_t samples[NUMSAMPLES];

///////////////////////////////////////////////PID////////////////////////////////////////////

long unsigned period = 0;
String inString = ""; 


//PID parameters//

double Kp=7, Ki=1, Kd=0;
double Setpoint, Input, Output; //These are just variables for storingvalues
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);
const int sampleRate = 500; 
unsigned long now = 0; 
unsigned long lastMessage = 0; 



void setup() {
    // Open serial communications and wait for port to open:
  Serial.begin(9600);
    //analogReference(EXTERNAL);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  
  }
  
  pinMode(PWM, OUTPUT);
  pinMode(DIR, OUTPUT);
  //analogWrite(PWM, Output);
  //digitalWrite(DIR, LOW);

  
/////////////////     PID library use    ////////////////////////

myPID.SetMode(AUTOMATIC); //Turn on the PID loop
myPID.SetSampleTime(sampleRate); //Sets the sample rate
myPID.SetOutputLimits(-255, 255);
Serial.println("Begin"); // Hello World!
Setpoint = 25; //Defult setpoint

}

void loop(){

  uint8_t i;
  float average;
 
  // take N samples in a row, with a slight delay
  for (i=0; i< NUMSAMPLES; i++) {
   samples[i] = analogRead(THERMISTORPIN);
   delay(50);
  }
 
  // average all the samples out
  average = 0;
  for (i=0; i< NUMSAMPLES; i++) {
     average += samples[i];
  }
  average /= NUMSAMPLES;
  

  // convert the value to resistance
  average = 1023 / average - 1;
  average = SERIESRESISTOR / average;

 //steinhart-hart temperature conversion formula
  float steinhart;
  steinhart = average / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C

  //Serial.println(analogRead(THERMISTORPIN)); 
  Serial.println(steinhart); 
  

  ////////////////////////////

  
  time = millis();
   // Read serial input:
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    if (isDigit(inChar)) {
      // convert the incoming byte to a char
      // and add it to the string:
      inString += (char)inChar;
    }
    // if you get a newline, print the string,
    // then the string's value:
    if (inChar == '\n') {
      Serial.print("Value:");
      Setpoint = inString.toInt();
      Serial.println(Setpoint);
      Serial.print("String: ");
      Serial.println(inString);
      
      // clear the string for new input:
      inString = "";
    }
  }

  Input = steinhart;  
  //period = millis();

  ///////////////setting temperature/////////////////

  myPID.SetTunings(Kp, Ki, Kd);
    myPID.SetControllerDirection(DIRECT);
    //Setpoint = 40;
    myPID.Compute();
    
    Serial.println(Output);
    
    if(Output > 0){
    digitalWrite(DIR, HIGH);
    analogWrite(PWM, Output);
  } else {
   digitalWrite(DIR, LOW);
   analogWrite(PWM, -Output);
   }   
 delay(50);
 
 }


    

 
